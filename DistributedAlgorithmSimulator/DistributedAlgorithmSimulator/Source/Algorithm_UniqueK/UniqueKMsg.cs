﻿using System;

/// <summary>
/// Distributed leader election algorithms in ring networks.
/// </summary>
namespace DistributedAlgorithms
{
    /// <summary>
    /// Message prototype for messages used in UniqueK algorithm.
    /// </summary>
    [Serializable] public class UniqueKMsg : WrapperClasses.Message
    {
        private int x;

        /// <summary>
        /// IDs of the originating node.
        /// </summary>
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        private int c;

        /// <summary>
        /// Counter.
        /// </summary>
        public int C
        {
            get { return c; }
            set { c = value; }
        }

        /// <summary>
        /// Initializes message values.
        /// </summary>
        /// <param name="ix">Node ID</param>
        /// <param name="ic">Counter value</param>
        public UniqueKMsg(int ix, int ic)
        {
            x = ix;
            c = ic;
        }
    }
}
