﻿using WrapperClasses;
using System;
using System.Collections.Generic;

/// <summary>
/// Distributed leader election algorithms in ring networks.
/// </summary>
namespace DistributedAlgorithms
{
    /// <summary>
    /// Contains UniqueK Algorithm actions.
    /// </summary>
    [Serializable]
    public class UniqueKNode : Node
    {
        private const int NoMessage = -1;

        /// <summary>
        /// An LCR algorithm message containing NOMESSAGE as its X value represents a non-message.
        /// i.e., the same as no message being sent through the channel.
        /// </summary>
        public static int NOMESSAGE
        {
            get { return NoMessage; }
        }

        private const int NoCount = -2;

        /// <summary>
        /// An undefined count state.
        /// </summary>
        public static int NOCOUNT
        {
            get { return NoCount; }
        }

        private const int Undefined = -3;

        /// <summary>
        /// Denotes an undefined state.
        /// </summary>
        public static int UNDEFINED
        {
            get { return Undefined; }
        }

        private bool init = false;

        /// <summary>
        /// Represents whether algorithm is initialized or not.
        /// </summary>
        public bool Init
        {
            get { return init; }
            set { init = value; }
        }

        private bool active = false;

        /// <summary>
        /// Represents whether the node is active or not.
        /// </summary>
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        private int k = 0;

        /// <summary>
        /// K, the maximum number of times a node ID is repeated in the ring.
        /// </summary>
        public int K
        {
            get { return k; }
            set { k = value; }
        }

        private bool leaderElected = false;

        /// <summary>
        /// True: the nodes knows that a leader has been elected by the algorithm. False: otherwise.
        /// </summary>
        public bool LeaderElected
        {
            get { return leaderElected; }
            set { leaderElected = value; }
        }

        private bool isLeader = false;

        /// <summary>
        /// True: if the node is the leader. False: otherwise.
        /// </summary>
        public bool IsLeader
        {
            get { return isLeader; }
            set { isLeader = value; }
        }

        // Elected leader's ID
        private int leader = -1;

        /// <summary>
        /// Elected leader's ID
        /// </summary>
        public int Leader
        {
            get { return leader; }
            set { leader = value; }
        }

        private int count = 0;

        /// <summary>
        /// P.count variable.
        /// </summary>
        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UniqueKNode"/> class.
        /// </summary>
        /// <param name="lid">Link ID.</param>
        /// <param name="cw">Clockwise neighbor ID.</param>
        /// <param name="ccw">Counter-clockwise neighbor ID.</param>
        public UniqueKNode(int lid, int cw, int ccw)
        {
            LinkId = lid;
            CwNeighbor = cw;
            CcwNeighbor = ccw;
            RcvdMsg = new UniqueKMsg(UniqueKNode.NOMESSAGE, UniqueKNode.NOCOUNT);
            SentMsg = new UniqueKMsg(UniqueKNode.NOMESSAGE, UniqueKNode.NOCOUNT);
        }

        /// <summary>
        /// Actions performed at each time step.
        /// This is the coded Actions Table.
        /// </summary>
        /// <param name="originalList">List of messages received by all nodes in the current step.</param>
        /// <param name="msgLst">List of messages that whill be sent by all nodes at the end of the current step.</param>
        public override void TimeTick(List<Message> originalList, ref List<Message> msgLst)
        {
            selfIndex = LinkId - 1;
            cwIndex = CwNeighbor - 1;
            ccwIndex = CcwNeighbor - 1;
            if (init == true)
            {
                // A1   Start
                Message msg = new UniqueKMsg(uid, 0);
                msgLst[cwIndex] = SentMsg = msg;
                init = false;
                hasStarted = true;
            }
            else if (hasStarted == true)
            {
                RcvdMsg = originalList[selfIndex];

                if (((UniqueKMsg)RcvdMsg).X > UniqueKNode.NOMESSAGE)
                {
                    // Node is inactive
                    if (active == false)
                    {
                        // A2   Passive Forward
                        if (((UniqueKMsg)RcvdMsg).X != uid && ((UniqueKMsg)RcvdMsg).C <= k)
                        {
                            msgLst[cwIndex] = SentMsg = RcvdMsg;
                        }

                        // A7   Terminate Message
                        if (((UniqueKMsg)RcvdMsg).X == uid)
                        {
                            // Equivalent of sending no MSG
                            msgLst[cwIndex] = SentMsg = new UniqueKMsg(UniqueKNode.NOMESSAGE, UniqueKNode.NOCOUNT);
                        }

                        // A10  Acknowledge Leader
                        if (((UniqueKMsg)RcvdMsg).C == k + 1)
                        {
                            msgLst[cwIndex] = SentMsg = RcvdMsg;
                            leader = ((UniqueKMsg)RcvdMsg).X;
                            leaderElected = true;
                        }
                    }
                    // Node is active
                    else
                    {
                        // A3   Active Forward
                        if (((UniqueKMsg)RcvdMsg).X != uid && (count == 0 || ((UniqueKMsg)RcvdMsg).C > count))
                        {
                            msgLst[cwIndex] = SentMsg = RcvdMsg;
                        }

                        // A4   Deactivate
                        if (((UniqueKMsg)RcvdMsg).X != uid && ((UniqueKMsg)RcvdMsg).C < count)
                        {
                            msgLst[cwIndex] = SentMsg = RcvdMsg;
                            active = false;
                        }

                        // A5   Forward Inferior
                        if (((UniqueKMsg)RcvdMsg).X < uid && ((UniqueKMsg)RcvdMsg).C >= 1 && count >= 1 && ((UniqueKMsg)RcvdMsg).C == count)
                        {
                            msgLst[cwIndex] = SentMsg = RcvdMsg;
                        }

                        // A6   Forward Superior
                        if (((UniqueKMsg)RcvdMsg).X > uid && ((UniqueKMsg)RcvdMsg).C >= 1 && count >= 1 && ((UniqueKMsg)RcvdMsg).C == count)
                        {
                            msgLst[cwIndex] = SentMsg = RcvdMsg;
                            active = false;
                        }

                        // A8   Increment Message
                        if (((UniqueKMsg)RcvdMsg).X == uid && ((UniqueKMsg)RcvdMsg).C <= k - 1 && count <= k - 1 && ((UniqueKMsg)RcvdMsg).C == count)
                        {
                            msgLst[cwIndex] = SentMsg = new UniqueKMsg(((UniqueKMsg)RcvdMsg).X, ((UniqueKMsg)RcvdMsg).C + 1);
                            count = ((UniqueKMsg)RcvdMsg).C + 1;
                        }

                        // A9   Elect Leader
                        if (((UniqueKMsg)RcvdMsg).C == k && ((UniqueKMsg)RcvdMsg).X == uid && count == k)
                        {
                            msgLst[cwIndex] = SentMsg = new UniqueKMsg(((UniqueKMsg)RcvdMsg).X, k + 1);
                            isLeader = true;
                            count = k + 1;
                        }

                        // A11  Finish
                        if (((UniqueKMsg)RcvdMsg).X == uid && ((UniqueKMsg)RcvdMsg).C == k + 1 && count == k + 1)
                        {
                            leaderElected = true;
                            leader = uid;
                            // Equivalent of sending no MSG
                            msgLst[cwIndex] = SentMsg = new UniqueKMsg(UniqueKNode.NOMESSAGE, UniqueKNode.NOCOUNT);
                        }
                    }
                }
                else
                {
                    msgLst[cwIndex] = SentMsg = new UniqueKMsg(UniqueKNode.NOMESSAGE, UniqueKNode.NOCOUNT);
                }
            }
        }
    }
}