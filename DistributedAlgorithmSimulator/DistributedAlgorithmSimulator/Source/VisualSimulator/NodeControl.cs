﻿using System;
using System.Drawing;
using System.Windows.Forms;

/// <summary>
/// This namespace includes classes used for development of the application.
/// </summary>
namespace DistributedAlgorithmSimulator
{
    /// <summary>
    /// A control that represents a node in the Visual Simulator.
    /// Consists of a circular shaped graphic that represents a node and text on it that represents node IDs.
    /// </summary>
    public class NodeControl : Control
    {
        private int DefSize = 30;
        private Graphics G = null;
        private Color NodeColor = Color.Black;
        private Color TextColor = Color.White;
        private int linkId = -1;
        private int ID = -1;

        /// <summary>
        /// Initializes an instance of a <see cref="NodeControl"/> class.
        /// </summary>
        /// <param name="nodeColor">Initial node color.</param>
        /// <param name="textColor">Node ID text color.</param>
        /// <param name="linkid">LinkId of the node.</param>
        /// <param name="id">Node ID of the node.</param>
        /// <param name="defSize">Node width (diameter).</param>
        public NodeControl(Color nodeColor, Color textColor, int linkid, int id, int defSize)
        {
            this.DoubleBuffered = true;
            NodeColor = nodeColor;
            TextColor = textColor;
            DefSize = defSize;
            ID = id;
            linkId = linkid;
        }

        /// <summary>
        /// Sets the node color.
        /// </summary>
        /// <param name="color">Node color.</param>
        public void SetNodeColor(Color color)
        {
            NodeColor = color;
        }

        /// <summary>
        /// Sets the node ID text color.
        /// </summary>
        /// <param name="color">Text color.</param>
        public void SetTextColor(Color color)
        {
            TextColor = color;
        }

        /// <summary>
        /// OnPaint event of the object.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            G = e.Graphics;
            G.SetHighQuality();
            DrawNode();
            DrawID();
        }

        /// <summary>
        /// On resize event of the object.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnResize(EventArgs e)
        {
            Invalidate();
            base.OnResize(e);
        }

        /// <summary>
        /// Draws a node control.
        /// </summary>
        protected void DrawNode()
        {
            G.FillEllipse(new SolidBrush(NodeColor), new Rectangle(0, 0, DefSize, DefSize));
        }

        /// <summary>
        /// Draws the node ID text.
        /// </summary>
        protected void DrawID()
        {
            string text = ID.ToString();
            G.DrawString(text, this.Font, new SolidBrush(TextColor), DefSize / 4, DefSize / 4);
        }
    }
}
