﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using WrapperClasses;
using DistributedAlgorithms;

/// <summary>
/// This namespace includes classes used for development of the application.
/// </summary>
namespace DistributedAlgorithmSimulator
{
    /// <summary>
    /// Visual Simulation window.
    /// </summary>
    public partial class VisualSimulator : Form
    {
        private static int NODE_WIDTH = 30;
        private static int NODE_HEIGHT = 30;
        private static int INFO_WIDTH = 40;
        private static int INFO_HEIGHT = 10;
        private static int NODE_INFO_GAP = 55;
        private static int MSG_WIDTH = 60;
        private static int NORMAL_BTN_W = 120;
        private static int SMALL_BTN_W = 50;
        private static int BTN_H = 30;
        private static int SMALL_BTN_W_GAP = NORMAL_BTN_W - (SMALL_BTN_W * 2);
        private static int BTN_H_GAP = 20;
        private static int DEFAULT_INTERVAL = 500;

        private const string LABEL_PLAY = "Play";
        private const string LABEL_PAUSE = "Pause";
        private const string LABEL_NEXT = "Next";
        private const string LABEL_PREV = "Previous";
        private const string LABEL_FIRST = "First";
        private const string LABEL_LAST = "Last";
        private const string LABEL_RESET = "Reset";
        private const string LABEL_SAVE = "Save to File";
        private const string LABEL_SAVE_NOSPACE = "SaveToFile";

        private const string PREFIX_NODE = "node";
        private const string PREFIX_NODE_INFO = "nodeInfo";
        private const string PREFIX_BUTTON = "btn";
        private const string PREFIX_CHECK_BOX = "chk";
        private const string PREFIX_LABEL = "lbl";
        private const string PREFIX_NUM_UP_DOWN = "numUpDown";
        private const string PREFIX_GROUP_BOX = "gb";
        private const string PREFIX_MSG = "msg";
        private const string PREFIX_FIXED_LABEL_VAL = "lblFixedValue";

        private const string FIELD_ACTIVE = "Active";
        private const string FIELD_IS_LEADER = "IsLeader";
        private const string FIELD_SENT_MSG = "SentMsg";
        private const string FIELD_RCVD_MSG = "RcvdMsg";

        private const string MSG_STOP = "Stop!";
        private const string MSG_END_SIM = "You are at the end of the simulation.";
        private const string MSG_BEGINNING_SIM = "You are at the beginning of the simulation.";

        private List<int> IdList = null;
        private List<Node> NodeList = null;
        private List<WrapperClasses.Message> MsgBuff = null;
        private int N;

        private List<int> oIdList = null;
        private List<Node> oNodeList = null;
        private List<WrapperClasses.Message> oMsgBuff = null;
        private int oN;
        private string oAlgorithm = String.Empty;

        private List<NodeState> StateList = null;
        private List<NodeState.Item> ItemList = null;
        private List<string> InfoList = null;
        private Utility util = null;

        private System.Threading.Timer t = null;
        private Object printLock = new Object();
        private int timeInterval = DEFAULT_INTERVAL;

        private bool RunFinished = false;

        private int totalSteps = 0;
        private int roundSteps = 0;
        private int roundNum = 1;

        private List<History> history = null;
        private int indexHist = 0;

        private int ringX;
        private int ringY;
        private int ringR;

        /// <summary>
        /// Initializes an instance of the <see cref="VisualSimulator"/> class.
        /// </summary>
        /// <param name="idList">Node ID list.</param>
        /// <param name="nodeList">Node list.</param>
        /// <param name="msgList">Message buffer.</param>
        /// <param name="n">Ring size in terms of nodes.</param>
        /// <param name="alg">Algorithm type.</param>
        public VisualSimulator(List<int> idList, List<WrapperClasses.Node> nodeList, List<WrapperClasses.Message> msgList, int n, string alg)
        {
            InitializeComponent();

            // Make new copies (with no references) of lists to be used after a reset.
            oIdList = idList.DeepClone();
            oNodeList = nodeList.DeepClone();
            oMsgBuff = msgList.DeepClone();
            oN = n;
            oAlgorithm = String.Copy(alg);

            // Keeps a record of each step in the execution. Used for navigation (Next, Prev, First, Last).
            history = new List<History>();

            // Creates an instance of the Utility class depending on the algorithm type.
            switch (oAlgorithm)
            {
                case Common.STR_ALG_LCR:
                    util = new LCRUtility();
                    break;
                case Common.STR_ALG_UNIQUE_K:
                    util = new UniqueKUtility();
                    break;
            }

            // Sets window title.
            SetText(gbSimulation, Common.STR_VISUAL_SIM_TITLE + oAlgorithm);

            // Gets the variables to be displayed.
            FillInfoList();

            // Dynamically create controls.
            CreateDynamicControls();

            // Sets playback buttons (Next, Prev, First, Last) to inactive.
            // Since navigation through an algorithm can only be done after running the execution once.
            SetStatusOfPlaybackButtons(false);

            // Refreshes all the variables to initial state.
            RefreshSimulator();
        }

        /// <summary>
        /// Calculates the XY-coordinates and the radius of the ring.
        /// </summary>
        private void CalculateRingProperties()
        {
            ringX = gbSimulation.Width / 2;
            ringY = gbSimulation.Height / 2 + 10;
            ringR = gbSimulation.Height / 2 - gbSimulation.Height / 6;
        }

        /// <summary>
        /// Gets the variables associated with the algorithm.
        /// </summary>
        private void FillInfoList()
        {
            ItemList = new List<NodeState.Item>();
            InfoList = new List<string>();

            ItemList = util.GetVariables();
            for (int i = 0; i < ItemList.Count; i++)
            {
                NodeState.Item it = ItemList[i];
                InfoList.Add(it.ItemName);
            }
        }

        /// <summary>
        /// Dyamically creates controls.
        /// </summary>
        private void CreateDynamicControls()
        {
            int xPos = 20;
            int yPos = 30;
            int lastYPos = yPos;

            // Create check boxes representing varibles.
            // Checking/unchecking them make variables be displayed or not during the execution.
            for (int i = 0; i < ItemList.Count; i++)
            {
                yPos = (i * NODE_HEIGHT) + NODE_HEIGHT;

                CheckBox chkBox = new CheckBox();
                chkBox.Name = PREFIX_CHECK_BOX + InfoList[i];
                chkBox.Location = new Point(xPos, yPos);
                chkBox.Checked = true;
                chkBox.CheckedChanged += new EventHandler(chkBox_CheckedChanged);

                Label lbl = new Label();
                lbl.Name = PREFIX_LABEL + InfoList[i];
                lbl.Text = InfoList[i];
                lbl.Location = new Point(xPos * 2, yPos + 2);

                AddControl(gbVariables, chkBox);
                AddControl(gbVariables, lbl);
                BringToFront(lbl);

                lastYPos = yPos;
            }

            // Group box to hold playback controls.
            GroupBox groupBox = new GroupBox();
            groupBox.Name = PREFIX_GROUP_BOX + "Playback";
            groupBox.Text = "Playback Controls";
            groupBox.Width = gbVariables.Width - 25;
            groupBox.Height = gbVariables.Height - lastYPos - (NODE_HEIGHT * 2) + 18;
            groupBox.Location = new Point(gbVariables.Location.X, lastYPos + (NODE_HEIGHT));
            AddControl(gbVariables, groupBox);

            lastYPos = lastYPos + NODE_HEIGHT;

            // NumericUpDown control to control the execution speed.
            Label lblNumUpDown = new Label();
            lblNumUpDown.Text = "Speed (ms)";
            lblNumUpDown.Location = new Point(gbVariables.Location.X + xPos + 8, lastYPos + NODE_HEIGHT);
            AddControl(gbVariables, lblNumUpDown);
            BringToFront(lblNumUpDown);

            lastYPos = lastYPos + BTN_H_GAP;

            NumericUpDown numUpDown = new NumericUpDown();
            numUpDown.Name = PREFIX_NUM_UP_DOWN + "Speed";
            numUpDown.Minimum = 500;
            numUpDown.Maximum = 5000;
            numUpDown.Increment = 500;
            numUpDown.Value = 1000;
            numUpDown.Location = new Point(gbVariables.Location.X + xPos + 8, lastYPos + NODE_HEIGHT);
            numUpDown.ValueChanged += new EventHandler(numUpDownSpeed_ValueChanged);
            AddControl(gbVariables, numUpDown);
            BringToFront(numUpDown);
            timeInterval = Convert.ToInt32(numUpDown.Value);

            lastYPos = lastYPos + NODE_HEIGHT;

            // Play/Pause button.
            Button btnPlayPause = new Button();
            btnPlayPause.Name = PREFIX_BUTTON + LABEL_PLAY + LABEL_PAUSE;
            btnPlayPause.Image = Properties.Resources.Play;
            btnPlayPause.Image.Tag = LABEL_PLAY;
            btnPlayPause.Width = NORMAL_BTN_W;
            btnPlayPause.Height = BTN_H;
            btnPlayPause.Location = new Point(groupBox.Location.X + xPos + 8, lastYPos + NODE_HEIGHT + BTN_H_GAP);
            btnPlayPause.Click += new EventHandler(btnPlayPause_Clicked);
            AddControl(gbVariables, btnPlayPause);
            BringToFront(btnPlayPause);

            lastYPos = lastYPos + NODE_HEIGHT + BTN_H_GAP;

            // Previous button.
            Button btnPrev = new Button();
            btnPrev.Name = PREFIX_BUTTON + LABEL_PREV;
            btnPrev.Image = Properties.Resources.Prev;
            btnPrev.Image.Tag = LABEL_PREV;
            btnPrev.Width = SMALL_BTN_W;
            btnPrev.Height = BTN_H;
            btnPrev.Location = new Point(groupBox.Location.X + xPos + 8, lastYPos + NODE_HEIGHT + BTN_H_GAP);
            btnPrev.Click += new EventHandler(btnPrev_Clicked);
            AddControl(gbVariables, btnPrev);
            BringToFront(btnPrev);

            // Next button.
            Button btnNext = new Button();
            btnNext.Name = PREFIX_BUTTON + LABEL_NEXT;
            btnNext.Image = Properties.Resources.Next;
            btnNext.Image.Tag = LABEL_NEXT;
            btnNext.Width = SMALL_BTN_W;
            btnNext.Height = BTN_H;
            btnNext.Location = new Point(groupBox.Location.X + xPos + 8 + btnPrev.Width + SMALL_BTN_W_GAP, lastYPos + NODE_HEIGHT + BTN_H_GAP);
            btnNext.Click += new EventHandler(btnNext_Clicked);
            AddControl(gbVariables, btnNext);
            BringToFront(btnNext);

            lastYPos = lastYPos + NODE_HEIGHT + BTN_H_GAP;

            // First button.
            Button btnFirst = new Button();
            btnFirst.Name = PREFIX_BUTTON + LABEL_FIRST;
            btnFirst.Image = Properties.Resources.EndBackward;
            btnFirst.Image.Tag = LABEL_FIRST;
            btnFirst.Width = SMALL_BTN_W;
            btnFirst.Height = BTN_H;
            btnFirst.Location = new Point(groupBox.Location.X + xPos + 8, lastYPos + NODE_HEIGHT + BTN_H_GAP);
            btnFirst.Click += new EventHandler(btnFirst_Clicked);
            AddControl(gbVariables, btnFirst);
            BringToFront(btnFirst);

            // Last button.
            Button btnLast = new Button();
            btnLast.Name = PREFIX_BUTTON + LABEL_LAST;
            btnLast.Image = Properties.Resources.EndForward;
            btnLast.Image.Tag = LABEL_LAST;
            btnLast.Width = SMALL_BTN_W;
            btnLast.Height = BTN_H;
            btnLast.Location = new Point(groupBox.Location.X + xPos + 8 + btnPrev.Width + SMALL_BTN_W_GAP, lastYPos + NODE_HEIGHT + BTN_H_GAP);
            btnLast.Click += new EventHandler(btnLast_Clicked);
            AddControl(gbVariables, btnLast);
            BringToFront(btnLast);

            lastYPos = lastYPos + NODE_HEIGHT + BTN_H_GAP;

            // Reset button.
            Button btnReset = new Button();
            btnReset.Name = PREFIX_BUTTON + LABEL_RESET;
            btnReset.Image = Properties.Resources.Reset;
            btnReset.Image.Tag = LABEL_RESET;
            btnReset.Width = NORMAL_BTN_W;
            btnReset.Height = BTN_H;
            btnReset.Location = new Point(groupBox.Location.X + xPos + 8, lastYPos + NODE_HEIGHT + BTN_H_GAP);
            btnReset.Click += new EventHandler(btnReset_Clicked);
            AddControl(gbVariables, btnReset);
            BringToFront(btnReset);

            lastYPos = lastYPos + NODE_HEIGHT + BTN_H_GAP;

            // Save to file button.
            Button btnSaveToFile = new Button();
            btnSaveToFile.Name = PREFIX_BUTTON + LABEL_SAVE_NOSPACE;
            btnSaveToFile.Text = LABEL_SAVE;
            btnSaveToFile.Width = NORMAL_BTN_W;
            btnSaveToFile.Height = BTN_H;
            btnSaveToFile.Location = new Point(groupBox.Location.X + xPos + 8, lastYPos + NODE_HEIGHT + BTN_H_GAP);
            btnSaveToFile.Click += new EventHandler(btnSaveToFile_Clicked);
            AddControl(gbVariables, btnSaveToFile);
            BringToFront(btnSaveToFile);
        }

        /// <summary>
        /// Draws nodes in the ring.
        /// </summary>
        /// <param name="isNavigate"><see langword="true"/>: navigation through history, <see langword="false"/>: real-time execution.</param>
        private void DrawNodes(bool isNavigate)
        {
            List<NodeState> stateList = null;
            int numNodes = NodeList.Count;

            // Angle between nodes.
            double theta = 360.0 / (double)numNodes;

            double xPos;
            double yPos;

            // Disposes previous controls, if any, before redrawing.
            foreach (Control ctrl in gbSimulation.Controls)
            {
                if (ctrl.GetType() == typeof(NodeControl))
                {
                    DisposeCtrl(ctrl);
                }
                if (ctrl.GetType() == typeof(NodeInfoControl))
                {
                    DisposeCtrl(ctrl);
                }
                if (ctrl.GetType() == typeof(Label))
                {
                    if (!ctrl.Name.StartsWith("lblFixed"))
                    {
                        DisposeCtrl(ctrl);
                    }
                }
            }

            // If navigation through a history, gets the appropriate state list for the current step.
            if (isNavigate)
            {
                stateList = history[indexHist].StateList;
            }
            // Real time execution.
            else
            {
                stateList = StateList;
            }

            // Goes through the node list and draws each node.
            for (int i = 0; i < numNodes; i++)
            {
                // Angle between nodes in radian.
                double radAngle = (Math.PI / 180) * (270 + (i * theta));

                // X and Y coordinates of nodes.
                xPos = (double)ringR * Math.Cos(radAngle) + ringX - (NODE_WIDTH / 2);
                yPos = (double)ringR * Math.Sin(radAngle) + ringY - (NODE_HEIGHT / 2);

                // Determine node color depending on the 'Active' and 'IsLeader' variable states.
                Color nodeColor = Color.Gray;
                bool active = (bool)stateList[i].ItemList.Find(x => x.ItemName.Contains(FIELD_ACTIVE)).ItemValue;
                if (active)
                {
                    nodeColor = Color.CornflowerBlue;
                }
                else
                {
                    nodeColor = Color.Red;
                }
                bool isLeader = (bool)stateList[i].ItemList.Find(x => x.ItemName.Contains(FIELD_IS_LEADER)).ItemValue;
                if (isLeader)
                {
                    nodeColor = Color.LawnGreen;
                }

                // Draws a NodeControl (Circle).
                NodeControl node = new NodeControl(nodeColor, Color.White, NodeList[i].LinkId, NodeList[i].UID, NODE_WIDTH);
                node.Name = PREFIX_NODE + (i + 1).ToString();
                node.Width = NODE_WIDTH;
                node.Height = NODE_HEIGHT;
                node.Location = new Point(Convert.ToInt32(xPos), Convert.ToInt32(yPos));
                AddControl(gbSimulation, node);
                BringToFront(node);

                // Draws a NodeInfoControl (Variable values).
                NodeInfoControl nInfo = new NodeInfoControl(InfoList, stateList[i], INFO_WIDTH * 2, INFO_HEIGHT, NodeList[i].LinkId, NodeList[i].UID);
                nInfo.Name = PREFIX_NODE_INFO + (i + 1).ToString();
                nInfo.Width = INFO_WIDTH * 2;
                nInfo.Height = InfoList.Count * INFO_HEIGHT;
                xPos = ((double)(ringR + NODE_INFO_GAP)) * Math.Cos(radAngle) + ringX - (NODE_WIDTH / 2);
                xPos = xPos - node.Width / 2;
                yPos = ((double)(ringR + NODE_INFO_GAP)) * Math.Sin(radAngle) + ringY - (NODE_HEIGHT / 2);
                yPos = yPos - node.Height / 2;
                nInfo.Location = new Point(Convert.ToInt32(xPos), Convert.ToInt32(yPos));
                AddControl(gbSimulation, nInfo);
                BringToFront(nInfo);

                // Draws a lable representing SentMsg in the current step.
                Label msg = new Label();
                msg.Name = PREFIX_MSG + (i + 1).ToString();
                List<string> msgVals = (List<string>)stateList[i].ItemList.Find(x => x.ItemName.Equals(FIELD_SENT_MSG)).ItemValue;
                string strMsg = string.Empty;
                if (msgVals.Count > 0)
                {
                    strMsg = "<";
                    foreach (string m in msgVals)
                    {
                        strMsg += m + ", ";
                    }
                    strMsg = strMsg.Remove(strMsg.Length - 2);
                    strMsg += ">";
                }
                msg.Text = strMsg;
                msg.Font = new Font(this.Font.FontFamily, 7, FontStyle.Bold);
                msg.Width = MSG_WIDTH;
                msg.Height = INFO_HEIGHT;
                xPos = ((double)(ringR - MSG_WIDTH)) * Math.Cos(radAngle) + ringX - (NODE_WIDTH / 2);
                xPos = xPos - 5;
                yPos = ((double)(ringR - 30)) * Math.Sin(radAngle) + ringY - msg.Height / 2;
                yPos = yPos - 3;
                msg.Location = new Point(Convert.ToInt32(xPos), Convert.ToInt32(yPos));
                AddControl(gbSimulation, msg);
                BringToFront(msg);

                BringToFront(node);
            }
        }

        /// <summary>
        /// Resets all varibles to initial state.
        /// </summary>
        private void RefreshSimulator()
        {
            string ERR_MSG = String.Empty;
            string PRINT_MSG = String.Empty;
            totalSteps = 0;
            roundNum = 1;
            history = new List<History>();

            RunFinished = false;

            // Reset the counters.
            Control.ControlCollection cc = gbSimulation.Controls;
            foreach (Control c in cc)
            {
                if (c.GetType() == typeof(Label) && c.Name.StartsWith(PREFIX_FIXED_LABEL_VAL))
                {
                    SetText(c, string.Empty);
                }
            }
            
            StateList = new List<NodeState>();
            IdList = oIdList.DeepClone();
            NodeList = oNodeList.DeepClone();
            MsgBuff = oMsgBuff.DeepClone();
            N = oN;

            if (!util.Initialize(N, ref NodeList, ref MsgBuff, ref ERR_MSG))
            {
                MessageBox.Show(ERR_MSG, "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            CalculateRingProperties();
            util.GetStatus(N, NodeList, ref StateList);

            UpdateInfoList();
            DrawNodes(false);
        }

        /// <summary>
        /// Updates the list of variables to be displayed depending on check status of check boxes.
        /// </summary>
        private void UpdateInfoList()
        {
            Control.ControlCollection cc = gbVariables.Controls;
            foreach (Control c in cc)
            {
                if (c.GetType() == typeof(CheckBox) && c.Name.StartsWith(PREFIX_CHECK_BOX))
                {
                    CheckBox cb = (CheckBox)c;
                    string cbName = cb.Name.Remove(0, PREFIX_CHECK_BOX.Length);

                    if (cb.Checked)
                    {
                        if (!InfoList.Contains(cbName))
                        {
                            InfoList.Add(cbName);
                        }
                    }
                    else
                    {
                        if (InfoList.Contains(cbName))
                        {
                            InfoList.Remove(cbName);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the checked status of check boxes.
        /// </summary>
        /// <param name="val"><see langword="true"/>: Checked, <see langword="false"/>: Unchecked.</param>
        private void SetCheckStatus(bool val)
        {
            Control.ControlCollection cc = gbVariables.Controls;
            foreach (Control c in cc)
            {
                if (c.GetType() == typeof(CheckBox) && c.Name.StartsWith(PREFIX_CHECK_BOX))
                {
                    ((CheckBox)c).Checked = val;
                }
            }
        }

        /// <summary>
        /// Sets the enabled statues of playback buttons.
        /// </summary>
        /// <param name="status"><see langword="true"/>: button enabled. <see langword="false"/>: button disapbled.</param>
        private void SetStatusOfPlaybackButtons(bool status)
        {
            Control.ControlCollection cc = gbVariables.Controls;
            foreach (Control c in cc)
            {
                if (c.GetType() == typeof(Button))
                {
                    if (c.Name == PREFIX_BUTTON + LABEL_NEXT)
                    {
                        EnableDisable(c, status);
                    }
                    else if (c.Name == PREFIX_BUTTON + LABEL_PREV)
                    {
                        EnableDisable(c, status);
                    }
                    else if (c.Name == PREFIX_BUTTON + LABEL_FIRST)
                    {
                        EnableDisable(c, status);
                    }
                    else if (c.Name == PREFIX_BUTTON + LABEL_LAST)
                    {
                        EnableDisable(c, status);
                    }
                }
            }
        }

        /// <summary>
        /// Sets the values of TotalSteps, RoundNumer, and Steps in the Round counters.
        /// </summary>
        /// <param name="roundNum">Round number.</param>
        /// <param name="roundSteps">Steps in the round.</param>
        /// <param name="totalSteps">Total steps.</param>
        private void SetCouners(string roundNum, string roundSteps, string totalSteps)
        {
            SetText(lblFixedValueN, N.ToString());
            SetText(lblFixedValueRound, roundNum);
            SetText(lblFixedValueSteps, roundSteps);
            SetText(lblFixedValueTotalSteps, totalSteps);
        }

        /// <summary>
        /// Saves the result of the execution to a file.
        /// </summary>
        /// <param name="outputString">Result of the execution in string format.</param>
        private void SaveData(ref string outputString)
        {
            string header = string.Empty;
            outputString = string.Empty;
            outputString += String.Format("N = {0}", N) + System.Environment.NewLine;
            outputString += String.Format("[ The ring in clockwise orientation ]") + System.Environment.NewLine;
            outputString += String.Format("-------------------------------------") + System.Environment.NewLine;

            for (int node = 0; node < N; node++)
            {
                outputString += String.Format("[{0}]  ", NodeList[node].UID);
            }
            outputString += System.Environment.NewLine;
            outputString += System.Environment.NewLine;

            header += String.Format("{0, -10}\t", "UID");
            List<NodeState.Item> varList = util.GetVariables();
            for (int v = 0; v < varList.Count; v++)
            {
                NodeState.Item it = varList[v];
                header += String.Format("{0, -10}\t", it.ItemName);
            }
            header += String.Format("{0, -10}\t", FIELD_RCVD_MSG) + FIELD_SENT_MSG;
            header += System.Environment.NewLine;
            header += String.Format("{0, -10}\t", "----------");
            for (int v = 0; v < varList.Count; v++)
            {
                header += String.Format("{0, -10}\t", "----------");
            }
            header += String.Format("{0, -10}\t", "----------") + "----------";
            header += System.Environment.NewLine;

            for (int i = 0; i < history.Count; i++)
            {
                History h = history[i];

                if (h.TotalSteps == 0)
                {
                    outputString += "Initial Configuration" + System.Environment.NewLine;
                }
                else
                {
                    outputString += String.Format("Step : {0}", h.TotalSteps) + System.Environment.NewLine;
                }

                outputString += header;

                List<NodeState> stateList = h.StateList;
                for (int j = 0; j < N; j++)
                {
                    List<NodeState.Item> itList = stateList[j].ItemList;
                    foreach (NodeState.Item it in itList)
                    {
                        if (it.ItemName == FIELD_RCVD_MSG || it.ItemName == FIELD_SENT_MSG)
                        {
                            List<string> msgVals = (List<string>)it.ItemValue;
                            string strMsg = string.Empty;
                            if (msgVals.Count > 0)
                            {
                                strMsg = "<";
                                foreach (string m in msgVals)
                                {
                                    strMsg += m + ", ";
                                }
                                strMsg = strMsg.Remove(strMsg.Length - 2);
                                strMsg += ">";
                            }
                            outputString += String.Format("{0, -10}\t", strMsg);
                        }
                        else
                        {
                            outputString += String.Format("{0, -10}\t", it.ItemValue.ToString());
                        }
                    }
                    outputString += System.Environment.NewLine;
                }
                outputString += System.Environment.NewLine;

                if (h.TotalSteps % N == 0 && h.TotalSteps != 0)
                {
                    outputString += String.Format("------------------------------ End of Round {0} ------------------------------", h.TotalSteps / N);
                    outputString += System.Environment.NewLine;
                    outputString += System.Environment.NewLine;
                }
            }
        }

        /// <summary>
        /// Performs one step of the execution of the algorith.
        /// Called from the <see cref="System.Threading.Timer"/>, at each time tick.
        /// </summary>
        /// <param name="o">Parameters. None here.</param>
        private void TimerCallback(Object o)
        {
            // Original represents the messages received at the current step.
            List<WrapperClasses.Message> Original = new List<WrapperClasses.Message>(N);
            for (int i = 0; i < N; i++)
            {
                Original.Add(MsgBuff[i]);
            }

            // This part has to be done in a lock. Otherwise print statement can get mixed up.
            lock (printLock)
            {
                // Calls the algorithm's each step execution method.
                // Original contains received messages, and the algorithm updates MsgBuff with the messages to be sent.
                for (int i = 0; i < N; i++)
                {
                    NodeList[i].TimeTick(Original, ref MsgBuff);
                }

                // Gets the status of the variables after above step.
                util.GetStatus(N, NodeList, ref StateList);
                
                // Draw nodes accordingly.
                DrawNodes(false);

                // Calculates and displays step and round numbers.
                roundSteps = totalSteps % N;
                if (roundSteps == 0 && totalSteps >= N)
                {
                    roundSteps = N;
                }                
                SetCouners(roundNum.ToString(), roundSteps.ToString(), totalSteps.ToString());

                // Adds an entry to the history.
                History h = new History();
                h.StateList = StateList;
                h.TotalSteps = totalSteps;
                h.RoundSteps = roundSteps;
                h.RoundNum = roundNum;
                history.Add(h);

                // Further calculations of steps.
                if (totalSteps % N == 0 && totalSteps != 0)
                {
                    roundNum++;
                }
                totalSteps = totalSteps + 1;
            }

            // Check whether the execution is finished.
            // If true, dispose the timer and do the necessary.
            if (util.IsFinished(N, NodeList))
            {
                t.Dispose();
                t = null;
                RunFinished = true;
                indexHist = totalSteps - 1;
                Control[] ctrls = gbVariables.Controls.Find(PREFIX_BUTTON + LABEL_PLAY + LABEL_PAUSE, true);
                SetImage((Button)ctrls[0], Properties.Resources.Play);
                SetImageTag((Button)ctrls[0], LABEL_PLAY);
                SetStatusOfPlaybackButtons(true);
            }
            else
            {
                if (o != null && (bool)o == true)
                {
                    t.Dispose();
                    t = null;
                }
            }
        }

        // Following are delegates to change controls from the TimerCallback thread.
        #region

        delegate void DisposeCtrlCallback(Control ctrl);
        private void DisposeCtrl(Control ctrl)
        {
            if (ctrl.InvokeRequired)
            {
                DisposeCtrlCallback d = new DisposeCtrlCallback(DisposeCtrl);
                this.Invoke(d, new object[] { ctrl });
            }
            else
            {
                ctrl.Dispose();
            }
        }

        delegate void AddControlCallback(GroupBox parent, Control child);
        private void AddControl(Control parent, Control child)
        {
            if (parent.InvokeRequired)
            {
                AddControlCallback d = new AddControlCallback(AddControl);
                this.Invoke(d, new object[] { parent, child });
            }
            else
            {
                parent.Controls.Add(child);
            }
        }

        delegate void BringToFrontCallback(Control ctrl);
        private void BringToFront(Control ctrl)
        {
            if (ctrl.InvokeRequired)
            {
                BringToFrontCallback d = new BringToFrontCallback(BringToFront);
                this.Invoke(d, new object[] { ctrl });
            }
            else
            {
                ctrl.BringToFront();
            }
        }

        delegate void SetTextCallback(Control ctrl, string text);
        private void SetText(Control ctrl, string text)
        {
            if (ctrl.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { ctrl, text });
            }
            else
            {
                ctrl.Text = text;
            }
        }

        delegate void SetImageCallback(Button btn, Image img);
        private void SetImage(Button btn, Image img)
        {
            if (btn.InvokeRequired)
            {
                SetImageCallback d = new SetImageCallback(SetImage);
                this.Invoke(d, new object[] { btn, img });
            }
            else
            {
                btn.Image = img;
            }
        }

        delegate void SetImageTagCallback(Button btn, string tag);
        private void SetImageTag(Button btn, string tag)
        {
            if (btn.InvokeRequired)
            {
                SetImageTagCallback d = new SetImageTagCallback(SetImageTag);
                this.Invoke(d, new object[] { btn, tag });
            }
            else
            {
                btn.Image.Tag = tag;
            }
        }

        delegate void EnableDisableCallback(Control ctrl, bool value);
        private void EnableDisable(Control ctrl, bool value)
        {
            if (ctrl.InvokeRequired)
            {
                EnableDisableCallback d = new EnableDisableCallback(EnableDisable);
                this.Invoke(d, new object[] { ctrl, value });
            }
            else
            {
                ctrl.Enabled = value;
            }
        }

        #endregion

        /// <summary>
        /// Play/Pause button click. Runs/Stops the execution.
        /// </summary>
        /// <param name="sender">[Play/Pause] button.</param>
        /// <param name="e">Event data.</param>
        private void btnPlayPause_Clicked(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            
            // Starts the execution.
            if (btn.Image.Tag.ToString() == LABEL_PLAY)
            {
                btn.Image = Properties.Resources.Pause;
                btn.Image.Tag = LABEL_PAUSE;
                if (RunFinished)
                {
                    if (t != null)
                    {
                        t.Dispose();
                        t = null;
                    }
                    RefreshSimulator();
                }
                t = new System.Threading.Timer(TimerCallback, null, 0, timeInterval);
            }
            // Pauses the execution.
            else if (btn.Image.Tag.ToString() == LABEL_PAUSE)
            {
                btn.Image = Properties.Resources.Play;
                btn.Image.Tag = LABEL_PLAY;
                if (t != null)
                {
                    t.Dispose();
                    t = null;
                }
            }
            SetStatusOfPlaybackButtons(false);
        }

        /// <summary>
        /// Resets all variables and counters.
        /// </summary>
        /// <param name="sender">[Reset] button.</param>
        /// <param name="e">Event data.</param>
        private void btnReset_Clicked(object sender, EventArgs e)
        {
            if (t != null)
            {
                t.Dispose();
                t = null;
            }

            RefreshSimulator();
            SetCheckStatus(true);
            Control[] playButton = gbVariables.Controls.Find(PREFIX_BUTTON + LABEL_PLAY + LABEL_PAUSE, true);
            if (((Button)playButton[0]).Image.Tag.ToString() == LABEL_PAUSE)
            {
                ((Button)playButton[0]).Image = Properties.Resources.Play;
                ((Button)playButton[0]).Image.Tag = LABEL_PLAY;
            }
            SetStatusOfPlaybackButtons(false);
        }

        /// <summary>
        /// Advances one step forward through the history.
        /// </summary>
        /// <param name="sender">[Next] playback button.</param>
        /// <param name="e">Event data.</param>
        private void btnNext_Clicked(object sender, EventArgs e)
        {
            if (indexHist == history.Count - 1)
            {
                MessageBox.Show(MSG_END_SIM, MSG_STOP, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                indexHist++;
                DrawNodes(true);
                SetCouners(history[indexHist].RoundNum.ToString(), history[indexHist].RoundSteps.ToString(), history[indexHist].TotalSteps.ToString());
            }
        }

        /// <summary>
        /// Advances one step backward through the history.
        /// </summary>
        /// <param name="sender">[Previous] playback button.</param>
        /// <param name="e">Event data.</param>
        private void btnPrev_Clicked(object sender, EventArgs e)
        {
            if (indexHist == 0)
            {
                MessageBox.Show(MSG_BEGINNING_SIM, MSG_STOP, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                indexHist--;
                DrawNodes(true);
                SetCouners(history[indexHist].RoundNum.ToString(), history[indexHist].RoundSteps.ToString(), history[indexHist].TotalSteps.ToString());
            }
        }

        /// <summary>
        /// Jumps to the first step of the execution.
        /// </summary>
        /// <param name="sender">[First] playback button.</param>
        /// <param name="e">Event data.</param>
        private void btnFirst_Clicked(object sender, EventArgs e)
        {
            if (indexHist == 0)
            {
                MessageBox.Show(MSG_BEGINNING_SIM, MSG_STOP, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                indexHist = 0;
                DrawNodes(true);
                SetCouners(history[indexHist].RoundNum.ToString(), history[indexHist].RoundSteps.ToString(), history[indexHist].TotalSteps.ToString());
            }
        }

        /// <summary>
        /// Jumps to the last step of the execution.
        /// </summary>
        /// <param name="sender">[Last] playback button.</param>
        /// <param name="e">Event data.</param>
        private void btnLast_Clicked(object sender, EventArgs e)
        {
            if (indexHist == history.Count - 1)
            {
                MessageBox.Show(MSG_END_SIM, MSG_STOP, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                indexHist = history.Count - 1;
                DrawNodes(true);
                SetCouners(history[indexHist].RoundNum.ToString(), history[indexHist].RoundSteps.ToString(), history[indexHist].TotalSteps.ToString());
            }
        }

        /// <summary>
        /// Saves the result of the execution to a file.
        /// </summary>
        /// <param name="sender">[Save to File] button.</param>
        /// <param name="e">Event data.</param>
        private void btnSaveToFile_Clicked(object sender, EventArgs e)
        {
            string RET_MSG = string.Empty;
            string ERR_MSG = string.Empty;

            SaveData(ref RET_MSG);

            if (!FileOperations.SaveToFile(Common.STR_DEFAULT_SAVE_FILE, RET_MSG, ref ERR_MSG))
            {
                MessageBox.Show(ERR_MSG, Common.CAPTION_FILE_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Update the list of variables to be displayed based on check status of check boxes.
        /// </summary>
        /// <param name="sender">Check box representing variables.</param>
        /// <param name="e">Event data.</param>
        private void chkBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkBox = (CheckBox)sender;
            string itemName = chkBox.Name.Remove(0, PREFIX_CHECK_BOX.Length);

            if (chkBox.Checked)
            {
                if (!InfoList.Contains(itemName))
                {
                    InfoList.Add(itemName);
                }
            }
            else
            {
                if (InfoList.Contains(itemName))
                {
                    InfoList.Remove(itemName);
                }
            }
            gbSimulation.Invalidate(true);
            DrawNodes(false);
        }

        /// <summary>
        /// Updates the execution speed depending on the user selected value.
        /// </summary>
        /// <param name="sender">[Speed] numeric up down control.</param>
        /// <param name="e">Event data.</param>
        private void numUpDownSpeed_ValueChanged(object sender, EventArgs e)
        {
            int oldInterval = timeInterval;
            timeInterval = Convert.ToInt32(((NumericUpDown)sender).Value);
            if (t != null)
            {
                t.Change(oldInterval, timeInterval);
            }
        }

        /// <summary>
        /// Draws the circle representing the ring.
        /// </summary>
        /// <param name="sender">Group box enclosing simulation area.</param>
        /// <param name="e">Event data.</param>
        private void gbSimulation_Paint(object sender, PaintEventArgs e)
        {
            Brush brush = new SolidBrush(Color.Gray);
            e.Graphics.DrawCircle(new Pen(brush), ringX, ringY, ringR);
        }

        /// <summary>
        /// Exits the Visual Simulator window.
        /// </summary>
        /// <param name="sender">[Exit] button.</param>
        /// <param name="e">Event data.</param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (t != null)
            {
                t.Dispose();
                t = null;
            }
            GC.Collect();
            this.Close();
        }
    }
}