﻿namespace DistributedAlgorithmSimulator
{
    partial class VisualSimulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisualSimulator));
            this.gbVariables = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.gbSimulation = new System.Windows.Forms.GroupBox();
            this.lblFixedValueN = new System.Windows.Forms.Label();
            this.lblFixedLabelN = new System.Windows.Forms.Label();
            this.lblFixedValueTotalSteps = new System.Windows.Forms.Label();
            this.lblFixedLabelTot = new System.Windows.Forms.Label();
            this.lblFixedValueSteps = new System.Windows.Forms.Label();
            this.lblFixedValueRound = new System.Windows.Forms.Label();
            this.lblFixedLabelStp = new System.Windows.Forms.Label();
            this.lblFixedLabelRnd = new System.Windows.Forms.Label();
            this.gbSimulation.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbVariables
            // 
            this.gbVariables.Location = new System.Drawing.Point(12, 13);
            this.gbVariables.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbVariables.Name = "gbVariables";
            this.gbVariables.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbVariables.Size = new System.Drawing.Size(202, 603);
            this.gbVariables.TabIndex = 0;
            this.gbVariables.TabStop = false;
            this.gbVariables.Text = "Select Variables to Display";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(1074, 808);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(118, 41);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(923, 623);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(99, 26);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // gbSimulation
            // 
            this.gbSimulation.Controls.Add(this.lblFixedValueN);
            this.gbSimulation.Controls.Add(this.lblFixedLabelN);
            this.gbSimulation.Controls.Add(this.lblFixedValueTotalSteps);
            this.gbSimulation.Controls.Add(this.lblFixedLabelTot);
            this.gbSimulation.Controls.Add(this.lblFixedValueSteps);
            this.gbSimulation.Controls.Add(this.lblFixedValueRound);
            this.gbSimulation.Controls.Add(this.lblFixedLabelStp);
            this.gbSimulation.Controls.Add(this.lblFixedLabelRnd);
            this.gbSimulation.Location = new System.Drawing.Point(220, 12);
            this.gbSimulation.Name = "gbSimulation";
            this.gbSimulation.Size = new System.Drawing.Size(802, 605);
            this.gbSimulation.TabIndex = 4;
            this.gbSimulation.TabStop = false;
            this.gbSimulation.Text = "Simulation";
            this.gbSimulation.Paint += new System.Windows.Forms.PaintEventHandler(this.gbSimulation_Paint);
            // 
            // lblFixedValueN
            // 
            this.lblFixedValueN.Location = new System.Drawing.Point(80, 21);
            this.lblFixedValueN.Name = "lblFixedValueN";
            this.lblFixedValueN.Size = new System.Drawing.Size(60, 22);
            this.lblFixedValueN.TabIndex = 7;
            this.lblFixedValueN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFixedLabelN
            // 
            this.lblFixedLabelN.Location = new System.Drawing.Point(26, 21);
            this.lblFixedLabelN.Name = "lblFixedLabelN";
            this.lblFixedLabelN.Size = new System.Drawing.Size(57, 22);
            this.lblFixedLabelN.TabIndex = 6;
            this.lblFixedLabelN.Text = "N :";
            this.lblFixedLabelN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFixedValueTotalSteps
            // 
            this.lblFixedValueTotalSteps.Location = new System.Drawing.Point(80, 87);
            this.lblFixedValueTotalSteps.Name = "lblFixedValueTotalSteps";
            this.lblFixedValueTotalSteps.Size = new System.Drawing.Size(60, 22);
            this.lblFixedValueTotalSteps.TabIndex = 5;
            this.lblFixedValueTotalSteps.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFixedLabelTot
            // 
            this.lblFixedLabelTot.Location = new System.Drawing.Point(2, 87);
            this.lblFixedLabelTot.Name = "lblFixedLabelTot";
            this.lblFixedLabelTot.Size = new System.Drawing.Size(82, 22);
            this.lblFixedLabelTot.TabIndex = 4;
            this.lblFixedLabelTot.Text = "Total Steps :";
            this.lblFixedLabelTot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFixedValueSteps
            // 
            this.lblFixedValueSteps.Location = new System.Drawing.Point(80, 65);
            this.lblFixedValueSteps.Name = "lblFixedValueSteps";
            this.lblFixedValueSteps.Size = new System.Drawing.Size(60, 22);
            this.lblFixedValueSteps.TabIndex = 3;
            this.lblFixedValueSteps.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFixedValueRound
            // 
            this.lblFixedValueRound.Location = new System.Drawing.Point(80, 43);
            this.lblFixedValueRound.Name = "lblFixedValueRound";
            this.lblFixedValueRound.Size = new System.Drawing.Size(60, 22);
            this.lblFixedValueRound.TabIndex = 2;
            this.lblFixedValueRound.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFixedLabelStp
            // 
            this.lblFixedLabelStp.Location = new System.Drawing.Point(27, 65);
            this.lblFixedLabelStp.Name = "lblFixedLabelStp";
            this.lblFixedLabelStp.Size = new System.Drawing.Size(57, 22);
            this.lblFixedLabelStp.TabIndex = 1;
            this.lblFixedLabelStp.Text = "Steps :";
            this.lblFixedLabelStp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFixedLabelRnd
            // 
            this.lblFixedLabelRnd.Location = new System.Drawing.Point(27, 43);
            this.lblFixedLabelRnd.Name = "lblFixedLabelRnd";
            this.lblFixedLabelRnd.Size = new System.Drawing.Size(57, 22);
            this.lblFixedLabelRnd.TabIndex = 0;
            this.lblFixedLabelRnd.Text = "Round :";
            this.lblFixedLabelRnd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VisualSimulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 661);
            this.Controls.Add(this.gbSimulation);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.gbVariables);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "VisualSimulator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simulation Display";
            this.gbSimulation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbVariables;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.GroupBox gbSimulation;
        private System.Windows.Forms.Label lblFixedValueSteps;
        private System.Windows.Forms.Label lblFixedValueRound;
        private System.Windows.Forms.Label lblFixedLabelStp;
        private System.Windows.Forms.Label lblFixedLabelRnd;
        private System.Windows.Forms.Label lblFixedValueTotalSteps;
        private System.Windows.Forms.Label lblFixedLabelTot;
        private System.Windows.Forms.Label lblFixedValueN;
        private System.Windows.Forms.Label lblFixedLabelN;
    }
}