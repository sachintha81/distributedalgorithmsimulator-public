﻿using WrapperClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

/// <summary>
/// This namespace includes classes used for development of the application.
/// </summary>
namespace DistributedAlgorithmSimulator
{
    /// <summary>
    /// A control that represents variables to be displayed alongside each node control.
    /// </summary>
    public class NodeInfoControl : Control
    {
        private const string PREFIX_LBL_NAME = "lblItemName_";
        private const string PREFIX_LBL_VAL = "lblItemValue_";

        private int DEFAULT_WIDTH = 0;
        private int DEFAULT_HEIGHT = 0;
        private int FONT_SIZE = 7;
        private Graphics G = null;
        private List<string> infoList = null;
        private int linkId = -1;
        private int ID = -1;
        private NodeState nodeState = null;

        /// <summary>
        /// Initializes an instance of a <see cref="NodeInfoControl"/> class.
        /// </summary>
        /// <param name="info">List of variables to be displayed.</param>
        /// <param name="state">Values of the variables to be displayed.</param>
        /// <param name="defaultW">Default width of the area where a variable is displayed.</param>
        /// <param name="defaultH">Default height of the area where a variable is displayed.</param>
        /// <param name="linkid">LinkID of the node.</param>
        /// <param name="id">Node ID of the node.</param>
        public NodeInfoControl(List<string> info, NodeState state, int defaultW, int defaultH, int linkid, int id)
        {
            this.DoubleBuffered = true;

            infoList = new List<string>();
            infoList = info;
            nodeState = state;

            DEFAULT_WIDTH = defaultW;
            DEFAULT_HEIGHT = defaultH;

            ID = id;
            linkId = linkid;
        }

        /// <summary>
        /// OnPaint event of the object.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            G = e.Graphics;
            G.SetHighQuality();
            DrawLabels();
        }

        /// <summary>
        /// OnResize event of the object.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnResize(EventArgs e)
        {
            Invalidate();
            base.OnResize(e);
        }

        /// <summary>
        /// Draws labels representing variables.
        /// </summary>
        protected void DrawLabels()
        {
            for (int i = 0; i < infoList.Count; i++)
            {
                // Label representing variable name.
                Label lblItemName = new Label();
                lblItemName.Name = PREFIX_LBL_NAME + infoList[i];
                lblItemName.Text = infoList[i] + " : ";
                lblItemName.Location = new Point(0, i * DEFAULT_HEIGHT);
                lblItemName.Font = new Font(this.Font.FontFamily, FONT_SIZE);
                this.Controls.Add(lblItemName);
                lblItemName.BringToFront();

                // Label representing variable value.
                Label lblItemValue = new Label();
                lblItemValue.Name = PREFIX_LBL_VAL + infoList[i];
                string value = nodeState.ItemList.Find(x => x.ItemName.Equals(infoList[i])).ItemValue.ToString();
                lblItemValue.Text = value;
                lblItemValue.Location = new Point(DEFAULT_WIDTH / 2, i * DEFAULT_HEIGHT);
                lblItemValue.Font = new Font(this.Font.FontFamily, FONT_SIZE, FontStyle.Bold);
                this.Controls.Add(lblItemValue);
                lblItemValue.BringToFront();
            }
        }
    }
}
