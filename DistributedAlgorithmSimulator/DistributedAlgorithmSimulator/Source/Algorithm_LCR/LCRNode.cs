﻿using WrapperClasses;
using System;
using System.Collections.Generic;

/// <summary>
/// Distributed leader election algorithms in ring networks.
/// </summary>
namespace DistributedAlgorithms
{
    /// <summary>
    /// Contains LCR Algorithm actions.
    /// </summary>
    [Serializable] public class LCRNode : Node
    {
        private const int NoMessage = -1;

        /// <summary>
        /// An LCR algorithm message containing NOMESSAGE as its X value represents a non-message.
        /// i.e., the same as no message being sent through the channel.
        /// </summary>
        public static int NOMESSAGE
        {
            get { return NoMessage; }
        }

        private bool init = false;

        /// <summary>
        /// Represents whether algorithm is initialized or not.
        /// </summary>
        public bool Init
        {
            get { return init; }
            set { init = value; }
        }

        private bool active = false;

        /// <summary>
        /// Represents whether the node is active or not.
        /// </summary>
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        private bool leaderElected = false;

        /// <summary>
        /// True: the nodes knows that a leader has been elected by the algorithm. False: otherwise.
        /// </summary>
        public bool LeaderElected
        {
            get { return leaderElected; }
            set { leaderElected = value; }
        }

        private bool isLeader = false;

        /// <summary>
        /// True: if the node is the leader. False: otherwise.
        /// </summary>
        public bool IsLeader
        {
            get { return isLeader; }
            set { isLeader = value; }
        }

        private int leader = -1;

        /// <summary>
        /// Elected leader's ID
        /// </summary>
        public int Leader
        {
            get { return leader; }
            set { leader = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LCRNode"/> class.
        /// </summary>
        /// <param name="lid">Link ID.</param>
        /// <param name="cw">Clockwise neighbor ID.</param>
        /// <param name="ccw">Counter-clockwise neighbor ID.</param>
        public LCRNode(int lid, int cw, int ccw)
        {
            LinkId = lid;
            CwNeighbor = cw;
            CcwNeighbor = ccw;
            RcvdMsg = new LCRMsg(NOMESSAGE, false);
            SentMsg = new LCRMsg(NOMESSAGE, false);
        }

        /// <summary>
        /// Actions performed at each time step.
        /// This is the coded Actions Table.
        /// </summary>
        /// <param name="originalList">List of messages received by all nodes in the current step.</param>
        /// <param name="msgLst">List of messages that whill be sent by all nodes at the end of the current step.</param>
        public override void TimeTick(List<Message> originalList, ref List<Message> msgLst)
        {
            selfIndex = LinkId - 1;
            cwIndex = CwNeighbor - 1;
            ccwIndex = CcwNeighbor - 1;
            if (init)
            {
                // A1: Start
                Message msg = new LCRMsg(uid, false);
                msgLst[cwIndex] = SentMsg = msg;
                init = false;
                hasStarted = true;
            }
            else if (hasStarted)
            {
                RcvdMsg = originalList[selfIndex];

                if (((LCRMsg)RcvdMsg).UID > NOMESSAGE)
                {
                    if (active)
                    {
                        // A2: Deactivate
                        // If a larger UID is seen, pass it along and deactivate self
                        if (((LCRMsg)RcvdMsg).UID > uid && !((LCRMsg)RcvdMsg).SP)
                        {
                            msgLst[cwIndex] = SentMsg = RcvdMsg;
                            active = false;
                        }

                        // A3: Terminate Message
                        // If a smaller UID is seen, do nothing
                        if (((LCRMsg)RcvdMsg).UID < uid && !((LCRMsg)RcvdMsg).SP)
                        {
                            msgLst[cwIndex] = SentMsg = new LCRMsg(NOMESSAGE, false);
                        }

                        // A4: Elect Leader
                        // If you receive your ID back, elect self as leader and pass the special leader elected message
                        if (((LCRMsg)RcvdMsg).UID == uid && !((LCRMsg)RcvdMsg).SP)
                        {
                            isLeader = true;
                            leader = uid;
                            leaderElected = true;
                            msgLst[cwIndex] = SentMsg = new LCRMsg(uid, true);
                        }
                        // A7: Finish
                        // When you receive the message in A3 back, finish.
                        else if (((LCRMsg)RcvdMsg).UID == uid && ((LCRMsg)RcvdMsg).SP)
                        {
                            active = false;
                        }

                    }
                    else
                    {
                        // A5: When the sapecial leader elected message received, set leader and leaderElected variables and pass the message
                        if (((LCRMsg)RcvdMsg).SP)
                        {
                            leader = ((LCRMsg)RcvdMsg).UID;
                            leaderElected = true;
                            msgLst[cwIndex] = SentMsg = RcvdMsg;
                        }
                        // A5: Passive Forward
                        // If inactive and receive a normal message simply pass it along
                        if (!((LCRMsg)RcvdMsg).SP)
                        {
                            msgLst[cwIndex] = SentMsg = RcvdMsg;
                        }
                    }
                }
                else
                {
                    // If no message received, do nothing
                    msgLst[cwIndex] = SentMsg = new LCRMsg(NOMESSAGE, false);
                }
            }
        }
    }
}
