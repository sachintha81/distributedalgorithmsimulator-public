﻿using WrapperClasses;
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Distributed leader election algorithms in ring networks.
/// </summary>
namespace DistributedAlgorithms
{
    /// <summary>
    /// A utility class that performs actions such as initializations and error checks.
    /// It acts as an interface to the main application which obtains the status of the algorithm at each step.
    /// </summary>
    public class LCRUtility : WrapperClasses.Utility
    {
        private const int UNDEFINED = -3;
        private const int CONFIRM = -4;

        private const int ERR_NO_UNIQUE = -100;
        private const int ERR_REPEAT = -101;

        private const string ERR_MSG_TOO_FEW_NODES = "The ring must contain at least 2 nodes.";
        private const string ERR_MSG_NO_UNIQUE_ID = "The ring must contain at least one unique ID.";
        private const string ERR_MSG_REPEAT_ID = "The ring must not contain repeating IDs.";
        private const string ERR_MSG_NODE_TYPE_ERR = "Incorrect Node Type.";
        private const string ERR_MSG_MSG_TYPE_ERR = "Incorrect Message Type.";
        private const string ERR_MSG_NEGATIVE_ID = "IDs must be non-negative.";
        private const string ERR_MSG_NON_NUMERIC_ID = "IDs must be non-negative.";

        /// <summary>
        /// Checks for errors.
        /// </summary>
        /// <returns><c>true</c>, if no errors, <c>false</c> otherwise.</returns>
        /// <param name="n">Number of nodes in the ring.</param>
        /// <param name="uidList">List of ID representing the nodes in the ring.</param>
        /// <param name="errMsg">Error message, if any. "SUCCESS" if successful.</param>
        override public bool ErrorChecks(int n, List<int> uidList, ref string errMsg)
        {
            int k = LCRUtility.UNDEFINED;

            // The ring must be of size 2 or more nodes
            if (n < 2)
            {
                errMsg = LCRUtility.ERR_MSG_TOO_FEW_NODES;
                return false;
            }

            // Negative UIDs aren't allowed
            foreach (int item in uidList)
            {
                if (item < 0)
                {
                    errMsg = LCRUtility.ERR_MSG_NEGATIVE_ID;
                    return false;
                }
            }

            try
            {
                k = FindK(uidList);
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }

            // All IDs must be unique
            if (k != 1)
            {
                switch (k)
                {
                    // There must be at least one unique ID
                    case LCRUtility.ERR_NO_UNIQUE:
                        errMsg = LCRUtility.ERR_MSG_NO_UNIQUE_ID;
                        break;
                    // There must not be repeating IDs
                    case LCRUtility.ERR_REPEAT:
                        errMsg = LCRUtility.ERR_MSG_REPEAT_ID;
                        break;
                }
                return false;
            }

            errMsg = LCRUtility.ERR_MSG_SUCCESS;
            return true;
        }

        /// <summary>
        /// Performs required initializations
        /// </summary>
        /// <param name="n">Size of the ring</param>
        /// <param name="nodeList">Node list</param>
        /// <param name="msgList">Message Buffer</param>
        /// <param name="errMsg">Error message, if any. "SUCCESS" if successful.</param>
        /// <returns><see langword="true"/>: Initialization successfu. <see langword="false"/>: otherwise.</returns>
        override public bool Initialize(int n, ref List<Node> nodeList, ref List<Message> msgList, ref string errMsg)
        {
            for (int i = 0; i < n; i++)
            {
                LCRNode node = (LCRNode)nodeList[i];
                node.Init = true;	// Initializes to true
                node.Active = true;	// Starts algorithm by activating nodes
                msgList.Add(new LCRMsg(LCRNode.NOMESSAGE, false));	// Add empty messages to message queue
            }

            errMsg = LCRUtility.ERR_MSG_SUCCESS;
            return true;
        }

        /// <summary>
        /// Checks whether the algorithm execution is finished.
        /// </summary>
        /// <returns><c>true</c> if executioin is finished; otherwise, <c>false</c>.</returns>
        /// <param name="n">Ring size</param>
        /// <param name="nodeList">Node list</param>
        override public bool IsFinished(int n, List<Node> nodeList)
        {
            bool finished = false;

            // If all the nodes have set their "LeaderElected" variable to true, algorithm is finished
            for (int i = 0; i < n; i++)
            {
                LCRNode node = (LCRNode)nodeList[i];
                if (node.Active == true)
                {
                    finished = false;
                    break;
                }
                finished = true;
            }

            return finished;
        }

        /// <summary>
        /// Prints the ring orientation in clockwise order.
        /// </summary>
        /// <returns><c>true</c>, if list was printed, <c>false</c> otherwise.</returns>
        /// <param name="n">Ring size</param>
        /// <param name="nodeList">Node list.</param>
        /// <param name="outputString">Ring orientation in string format.</param>
        /// <param name="errMsg">Error message, if any. "SUCCESS" if successful.</param>
        override public bool PrintList(int n, List<Node> nodeList, ref string outputString, ref string errMsg)
        {
            string outString = String.Empty;
            try
            {
                outString += String.Format("N = {0}", n) + System.Environment.NewLine;
                outString += String.Format("[ The ring in clockwise orientation ]") + System.Environment.NewLine;
                outString += String.Format("-------------------------------------") + System.Environment.NewLine;

                for (int i = 0; i < n; i++)
                {
                    outString += String.Format("[{0}]  ", nodeList[i].UID);
                }
                outString += System.Environment.NewLine;
                outString += System.Environment.NewLine;

                outputString = outString;
                errMsg = LCRUtility.ERR_MSG_SUCCESS;
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Returns (as a reference) the state of all the nodes as a single string.
        /// </summary>
        /// <param name="n">Ring size</param>
        /// <param name="step">The step number to be printed.</param>
        /// <param name="nodeList">Node list.</param>
        /// <param name="outputString">The state of all the nodes as a single string.</param>
        /// <param name="errMsg">Error message, if any. "SUCCESS" if successful.</param>
        override public void PrintStep(int n, int step, List<Node> nodeList, ref string outputString, ref string errMsg)
        {
            string outString = String.Empty;
            string padded = string.Empty;

            try
            {
                if (step == 0)
                {
                    outString += "Initial Configuration" + System.Environment.NewLine;
                }
                else
                {
                    outString += String.Format("Step : {0}", step) + System.Environment.NewLine;
                }

                padded = String.Format("{0, -7}\t{1, -7}\t{2, -7}\t{3, -7}\t{4, -7}\t{5, -7}\t{6, -7}\t",
                                    "UID", "Active", "Elected", "IsLeader", "Leader", "RcvdMsg", "SentMsg");
                outString += padded;
                outString += System.Environment.NewLine;
                padded = String.Format("{0, -8}\t{1, -8}\t{2, -8}\t{3, -8}\t{4, -8}\t{5, -8}\t{6, -8}\t",
                                        "----------", "----------", "----------", "----------", "----------", "----------", "----------");
                outString += padded;
                outString += System.Environment.NewLine;

                for (int i = 0; i < n; i++)
                {
                    LCRNode node = (LCRNode)nodeList[i];
                    outString += String.Format("{0, -10}\t{1, -10}\t{2, -10}\t{3, -10}\t{4, -10}\t<{5}, {6}>\t<{7}, {8}>",
                        node.UID,
                        node.Active,
                        node.LeaderElected,
                        node.IsLeader,
                        node.Leader,
                        ((LCRMsg)(node.RcvdMsg)).UID, (((LCRMsg)(node.RcvdMsg)).SP).ToString().Substring(0, 1),
                        ((LCRMsg)(node.SentMsg)).UID, (((LCRMsg)(node.SentMsg)).SP).ToString().Substring(0, 1));
                    outString += System.Environment.NewLine;
                }
                outString += System.Environment.NewLine;

                if (step % n == 0 && step != 0)
                {
                    outString += String.Format("------------------------------ End of Round {0} ------------------------------", step / n);
                    outString += System.Environment.NewLine;
                    outString += System.Environment.NewLine;
                }

                outputString = outString;
                errMsg = ERR_MSG_SUCCESS;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return;
            }
        }

        /// <summary>
        /// Returns (as a reference) the state of all the nodes as a <see cref="NodeState"/> list.
        /// </summary>
        /// <param name="n">Ring size</param>
        /// <param name="nodeList">Node list</param>
        /// <param name="stateList">List of node states</param>
        override public void GetStatus(int n, List<Node> nodeList, ref List<NodeState> stateList)
        {
            stateList = new List<NodeState>(n);

            for (int i = 0; i < n; i++)
            {
                LCRNode node = (LCRNode)nodeList[i];
                NodeState state = new NodeState(node.LinkId, node.CwNeighbor, node.CcwNeighbor);

                NodeState.Item UID = new NodeState.Item();
                UID.ItemName = "UID";
                UID.ItemValue = node.UID;
                state.AddItem(UID);

                NodeState.Item Active = new NodeState.Item();
                Active.ItemName = "Active";
                Active.ItemValue = node.Active;
                state.AddItem(Active);

                NodeState.Item LeaderElected = new NodeState.Item();
                LeaderElected.ItemName = "Elected";
                LeaderElected.ItemValue = node.LeaderElected;
                state.AddItem(LeaderElected);

                NodeState.Item IsLeader = new NodeState.Item();
                IsLeader.ItemName = "IsLeader";
                IsLeader.ItemValue = node.IsLeader;
                state.AddItem(IsLeader);

                NodeState.Item Leader = new NodeState.Item();
                Leader.ItemName = "Leader";
                Leader.ItemValue = node.Leader;
                state.AddItem(Leader);

                NodeState.Item RcvdMsg = new NodeState.Item();
                RcvdMsg.ItemName = "RcvdMsg";
                List<string> rMsgItems = new List<string>();
                LCRMsg mR = (LCRMsg)(node.RcvdMsg);
                rMsgItems.Add(mR.UID.ToString());
                rMsgItems.Add(mR.SP.ToString());
                RcvdMsg.ItemValue = rMsgItems;
                state.AddItem(RcvdMsg);

                NodeState.Item SentMsg = new NodeState.Item();
                SentMsg.ItemName = "SentMsg";
                List<string> sMsgItems = new List<string>();
                LCRMsg mS = (LCRMsg)(node.SentMsg);
                if (mS.UID != LCRNode.NOMESSAGE)
                {
                    sMsgItems.Add(mS.UID.ToString());
                    sMsgItems.Add(mS.SP.ToString());
                }
                SentMsg.ItemValue = sMsgItems;
                state.AddItem(SentMsg);

                stateList.Add(state);
            }
        }

        /// <summary>
        /// Returns a list of variables used in the algorithm.
        /// </summary>
        /// <returns>List of variables used in the algorithm.</returns>
        public override List<NodeState.Item> GetVariables()
        {
            List<NodeState.Item> list = new List<NodeState.Item>();

            NodeState.Item Active = new NodeState.Item();
            Active.ItemName = "Active";
            list.Add(Active);

            NodeState.Item LeaderElected = new NodeState.Item();
            LeaderElected.ItemName = "Elected";
            list.Add(LeaderElected);


            NodeState.Item IsLeader = new NodeState.Item();
            IsLeader.ItemName = "IsLeader";
            list.Add(IsLeader);

            NodeState.Item Leader = new NodeState.Item();
            Leader.ItemName = "Leader";
            list.Add(Leader);

            return list;
        }

        /// <summary>
        /// Finds the maximum number of repeating IDs in a list of integers
        /// </summary>
        /// <returns>Maximum number of repeating IDs</returns>
        /// <param name="list">ID list</param>
        public static int FindK(List<int> list)
        {
            int k;
            var dict = new Dictionary<int, int>();

            foreach (var value in list)
            {
                if (dict.ContainsKey(value))
                    dict[value]++;
                else
                    dict[value] = 1;
            }

            if (!dict.Values.Contains(1))
            {
                k = LCRUtility.ERR_NO_UNIQUE;
            }
            else if (dict.Values.Max() != 1)
            {
                k = LCRUtility.ERR_REPEAT;
            }
            else
            {
                k = dict.Values.Max();
            }

            return k;
        }
    }
}
