﻿using System;

/// <summary>
/// Distributed leader election algorithms in ring networks.
/// </summary>
namespace DistributedAlgorithms
{
    /// <summary>
    /// Message prototype for messages used in LCR algorithm.
    /// </summary>
    [Serializable] public class LCRMsg : WrapperClasses.Message
    {
        private int uid;

        /// <summary>
        /// Unique IDs of nodes. Non-negative values.
        /// </summary>
        public int UID
        {
            get { return uid; }
            set { uid = value; }
        }

        private bool sp;

        /// <summary>
        /// Indicates whether a message is a special message or not.
        /// True: special message, False: regular message.
        /// </summary>
        public bool SP
        {
            get { return sp; }
            set { sp = value; }
        }

        /// <summary>
        /// Initializes message values.
        /// </summary>
        /// <param name="u">Unique ID</param>
        /// <param name="s">Special message or not</param>
        public LCRMsg(int u, bool s)
        {
            uid = u;
            sp = s;
        }
    }
}
