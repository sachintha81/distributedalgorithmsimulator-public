﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using WrapperClasses;
using DistributedAlgorithms;

/// <summary>
/// This namespace includes classes used for development of the application.
/// </summary>
namespace DistributedAlgorithmSimulator
{
    /// <summary>
    /// Main Windows of the Distributed Algorithm Simulator application.
    /// </summary>
    public partial class DistributedAlgorithmSimulator : Form
    {
        private const int MAX_NODES_VISUAL_SIM = 18;
        private const string ERR_MSG_INVALID_FILE_NAME = "Invalid file name. Please enter a valid file name.";
        private const string ERR_MSG_NO_IDS = "You must enter node IDs.";
        private static string MSG_TEXT_SIM = String.Format("Cannot run the Visual Simulator because n > {0}. \nWould you like to run a Textual Simulation instead?", MAX_NODES_VISUAL_SIM);

        private const string DEFAULT_IN_FILE = "Input.csv";
        
        // A list containing UIDs. Read from a file.
        private List<int> IdList = null;

        // Buffer that contains messages sent each time step
        private List<WrapperClasses.Message> MessageBuff = null;

        // List of Nodes
        private List<Node> NodeList = null; 

        // Size of the network
        private int n;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributedAlgorithmSimulator"/> class.
        /// </summary>
        public DistributedAlgorithmSimulator()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Performs initializations such as setting initial values of controls.
        /// </summary>
        private void Initializations()
        {
            // Default input file name: Input.csv
            txtOpenFile.Text = DEFAULT_IN_FILE;

            // Set default read option to read from user input textbox
            rbInputIDs.Checked = true;
            rbReadFromFile.Checked = false;

            // Set default simulation type to Visual Simulation
            rbVisualSim.Checked = true;
            rbTextualSim.Checked = false;

            btnRun.Enabled = false;
            LoadAlgorithms();
        }

        /// <summary>
        /// Creates a ring network which can pass messages in either clockwise or counter-clockwise directions
        /// Nodes are linked to their CW and CCW neighbors using a private id called linkID (which isn't a part of the algorithm)
        /// </summary>
        /// <returns><c>true</c>, if network was successfully created, <c>false</c> otherwise.</returns>
        /// <param name="algorithm">Algorithm type</param>
        /// <param name="errMsg">Error message. "SUCCESS" if no error.</param>
        private bool CreateNetwork(string algorithm, ref string errMsg)
        {
            try
            {
                NodeList = new List<Node>(n);

                // Nodes in the ring are assigned "linkId" 1 through n
                // Nodes are linked using them, each node knows its CW and CCW neighbors
                // Eg.
                // if n = 5, then node 1 has CW neighbor = 2 and CCW neighbor = 5
                // if n = 5, then node 5 has CW neighbor = 1 and CCW neighbor = 4
                for (int i = 1; i <= n; i++)
                {
                    int linkId = i;
                    int cw = linkId + 1;
                    if (cw > n)
                    {
                        cw = 1;
                    }
                    int ccw = linkId - 1;
                    if (ccw < 1)
                    {
                        ccw = n;
                    }

                    Node newNode = null;
                    switch (algorithm)
                    {
                        case Common.STR_ALG_UNIQUE_K:
                            newNode = new UniqueKNode(linkId, cw, ccw);
                            break;
                        case Common.STR_ALG_LCR:
                            newNode = new LCRNode(linkId, cw, ccw);
                            break;
                    }

                    // Each node is assigned the actual UIDs read from the file (different from linkId)
                    newNode.UID = IdList[i - 1];
                    NodeList.Add(newNode);
                }
                errMsg = Common.ERR_MSG_SUCCESS;
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Launches either a Visual Simulation or a Textual Simulation of an algorithm of user's choice.
        /// </summary>
        /// <param name="algorithm">Algorithm type.</param>
        /// <param name="isVisual">Simulation type.</param>
        private void LaunchSimulation(string algorithm, bool isVisual)
        {
            string ERR_MSG = String.Empty;
            MessageBuff = new List<WrapperClasses.Message>();

            // Instantiate a Utility instance based on algorithm type
            Utility util = null;
            switch (algorithm)
            {
                case Common.STR_ALG_LCR:
                    util = new LCRUtility();
                    break;

                case Common.STR_ALG_UNIQUE_K:
                    util = new UniqueKUtility();
                    break;
            }

            // Perform eror checks
            if (!util.ErrorChecks(n, IdList, ref ERR_MSG))
            {
                MessageBox.Show(ERR_MSG, Common.CAPTION_INVALID_ID, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                // Creates the network
                if (!CreateNetwork(algorithm, ref ERR_MSG))
                {
                    MessageBox.Show(ERR_MSG, Common.CAPTION_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    // Launching visual/textual simulation
                    if (isVisual)
                    {
                        VisualSimulator dispWindow = new VisualSimulator(IdList, NodeList, MessageBuff, n, algorithm);
                        dispWindow.ShowDialog();
                    }
                    else
                    {
                        TextualSimulator dispWindow = new TextualSimulator(IdList, NodeList, MessageBuff, n, algorithm);
                        dispWindow.ShowDialog();
                    }
                }
            }
        }

        /// <summary>
        /// Loads available algorithms into "Algorithm Type" combo box.
        /// </summary>
        private void LoadAlgorithms()
        {
            cmbAlgorithm.Items.Add(Algorithm.LCR);
            cmbAlgorithm.Items.Add(Algorithm.UniqueK);
            cmbAlgorithm.SelectedIndex = 0;
        }

        /// <summary>
        /// Form load event of the main window.
        /// </summary>
        /// <param name="sender">Main Window.</param>
        /// <param name="e">Event data.</param>
        private void SimulatorMain_Load(object sender, EventArgs e)
        {
            Initializations();
        }

        /// <summary>
        /// Reads the node IDs and creates a list of IDs.
        /// Depending on the user's selection, reads either from a user selected file or takes input from a text box.
        /// </summary>
        /// <param name="sender">[Read] button.</param>
        /// <param name="e">Event data.</param>
        private void btnRead_Click(object sender, EventArgs e)
        {
            string ERR_MSG = String.Empty;

            btnRun.Enabled = false;
            if (rbReadFromFile.Checked)
            {
                if (txtOpenFile.Text == String.Empty)
                {
                    MessageBox.Show(ERR_MSG_INVALID_FILE_NAME, Common.CAPTION_FILE_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    IdList = new List<int>();

                    // Read from a CSV file
                    if (!FileOperations.ReadCSVFile(txtOpenFile.Text, ref IdList, ref ERR_MSG))
                    {
                        MessageBox.Show(ERR_MSG, Common.CAPTION_FILE_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        // Put node IDs in a list of integers
                        n = IdList.Count;
                        string idString = String.Empty;
                        foreach (int id in IdList)
                        {
                            idString += id + ",";
                        }
                        if (String.Compare(idString[idString.Length - 1].ToString(), ",") == 0)
                        {
                            txtTextSimDisp.Text = idString.Remove(idString.Length - 1, 1);
                        }
                        else
                        {
                            txtTextSimDisp.Text = idString;
                        }
                        btnRun.Enabled = true;
                    }
                }
            }
            else if (rbInputIDs.Checked)
            {
                if (txtInputIDs.Text == string.Empty)
                {
                    MessageBox.Show(ERR_MSG_NO_IDS, Common.CAPTION_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    // Read from a textbox
                    IdList = new List<int>();
                    try
                    {
                        char[] ch = new char[] { ',' };
                        string[] ids = txtInputIDs.Text.Split(ch, StringSplitOptions.RemoveEmptyEntries);
                        string idString = String.Empty;

                        foreach (string id in ids)
                        {
                            IdList.Add(Convert.ToInt32(id));
                            idString += id + ",";
                        }
                        n = IdList.Count;

                        if (String.Compare(idString[idString.Length - 1].ToString(), ",") == 0)
                        {
                            txtTextSimDisp.Text = idString.Remove(idString.Length - 1, 1);
                        }
                        else
                        {
                            txtTextSimDisp.Text = idString;
                        }
                        btnRun.Enabled = true;
                    }
                    catch(Exception ex)
                    {
                        IdList = null;
                        MessageBox.Show(ex.Message, Common.CAPTION_FILE_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        /// <summary>
        /// Specifies user preference of reading input from a file.
        /// </summary>
        /// <param name="sender">[Read from File] radio button.</param>
        /// <param name="e">Event data.</param>
        private void rbReadFromFile_CheckedChanged(object sender, EventArgs e)
        {
            if (rbReadFromFile.Checked)
            {
                gbReadFromFile.Enabled = true;
                gbInputIDs.Enabled = false;
            }
        }

        /// <summary>
        /// Specifies user preference of reading input from a textbox.
        /// </summary>
        /// <param name="sender">[Input IDs] radio button.</param>
        /// <param name="e">Event data.</param>
        private void rbInputIDs_CheckedChanged(object sender, EventArgs e)
        {
            if (rbInputIDs.Checked)
            {
                gbInputIDs.Enabled = true;
                gbReadFromFile.Enabled = false;
            }
        }

        /// <summary>
        /// Launches a FileOpen dialog which lets the user select the input file.
        /// </summary>
        /// <param name="sender">[Select] button.</param>
        /// <param name="e">Event data.</param>
        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileNodeIds.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtOpenFile.Text = openFileNodeIds.FileName;
            }
        }

        /// <summary>
        /// Executes the simulation depending on user preference of Visual or Textual simulation. 
        /// If the number of nodes is greater than 18, prompts the user to launch a Textual Simulation.
        /// </summary>
        /// <param name="sender">[RUN SIMULATION] button.</param>
        /// <param name="e">Event data.</param>
        private void btnRun_Click(object sender, EventArgs e)
        {
            // Select algorithm based on user choice
            string algorithm = string.Empty;
            if (cmbAlgorithm.SelectedIndex == Convert.ToInt32(Algorithm.LCR))
            {
                algorithm = Common.STR_ALG_LCR;
            }
            else if (cmbAlgorithm.SelectedIndex == Convert.ToInt32(Algorithm.UniqueK))
            {
                algorithm = Common.STR_ALG_UNIQUE_K;
            }

            // Launch Textual Simulation
            if (rbTextualSim.Checked)
            {
                LaunchSimulation(algorithm, false);
            }
            // Launch Visual Simulation
            else if (rbVisualSim.Checked)
            {
                // If the number of nodes > 18, propmpt the user to launch a Textual Simulation
                if (n > MAX_NODES_VISUAL_SIM)
                {
                    if (MessageBox.Show(MSG_TEXT_SIM, Common.CAPTION_TEXT_SIM, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        LaunchSimulation(algorithm, false);
                    }
                }
                else
                {
                    LaunchSimulation(algorithm, true);
                }
            }
        }

        /// <summary>
        /// Exits the application.
        /// </summary>
        /// <param name="sender">[Exit] button.</param>
        /// <param name="e">Event data.</param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
