﻿namespace DistributedAlgorithmSimulator
{
    partial class DistributedAlgorithmSimulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DistributedAlgorithmSimulator));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.gbInputIDs = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInputIDs = new System.Windows.Forms.TextBox();
            this.rbInputIDs = new System.Windows.Forms.RadioButton();
            this.gbReadFromFile = new System.Windows.Forms.GroupBox();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.ile = new System.Windows.Forms.Label();
            this.txtOpenFile = new System.Windows.Forms.TextBox();
            this.rbReadFromFile = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRun = new System.Windows.Forms.Button();
            this.cmbAlgorithm = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rbTextualSim = new System.Windows.Forms.RadioButton();
            this.rbVisualSim = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTextSimDisp = new System.Windows.Forms.TextBox();
            this.btnRead = new System.Windows.Forms.Button();
            this.openFileNodeIds = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.gbInputIDs.SuspendLayout();
            this.gbReadFromFile.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnExit);
            this.groupBox1.Controls.Add(this.gbInputIDs);
            this.groupBox1.Controls.Add(this.rbInputIDs);
            this.groupBox1.Controls.Add(this.gbReadFromFile);
            this.groupBox1.Controls.Add(this.rbReadFromFile);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnRead);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(660, 535);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Leader Election in Ring Networks";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(542, 503);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(106, 25);
            this.btnExit.TabIndex = 17;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // gbInputIDs
            // 
            this.gbInputIDs.Controls.Add(this.label1);
            this.gbInputIDs.Controls.Add(this.txtInputIDs);
            this.gbInputIDs.Location = new System.Drawing.Point(6, 104);
            this.gbInputIDs.Name = "gbInputIDs";
            this.gbInputIDs.Size = new System.Drawing.Size(648, 53);
            this.gbInputIDs.TabIndex = 16;
            this.gbInputIDs.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Input IDs";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtInputIDs
            // 
            this.txtInputIDs.Location = new System.Drawing.Point(72, 16);
            this.txtInputIDs.Name = "txtInputIDs";
            this.txtInputIDs.Size = new System.Drawing.Size(570, 25);
            this.txtInputIDs.TabIndex = 5;
            // 
            // rbInputIDs
            // 
            this.rbInputIDs.AutoSize = true;
            this.rbInputIDs.Location = new System.Drawing.Point(6, 25);
            this.rbInputIDs.Name = "rbInputIDs";
            this.rbInputIDs.Size = new System.Drawing.Size(77, 21);
            this.rbInputIDs.TabIndex = 1;
            this.rbInputIDs.TabStop = true;
            this.rbInputIDs.Text = "Input IDs";
            this.rbInputIDs.UseVisualStyleBackColor = true;
            this.rbInputIDs.CheckedChanged += new System.EventHandler(this.rbInputIDs_CheckedChanged);
            // 
            // gbReadFromFile
            // 
            this.gbReadFromFile.Controls.Add(this.btnOpenFile);
            this.gbReadFromFile.Controls.Add(this.ile);
            this.gbReadFromFile.Controls.Add(this.txtOpenFile);
            this.gbReadFromFile.Location = new System.Drawing.Point(6, 45);
            this.gbReadFromFile.Name = "gbReadFromFile";
            this.gbReadFromFile.Size = new System.Drawing.Size(648, 53);
            this.gbReadFromFile.TabIndex = 14;
            this.gbReadFromFile.TabStop = false;
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(536, 17);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(106, 25);
            this.btnOpenFile.TabIndex = 4;
            this.btnOpenFile.Text = "Select";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // ile
            // 
            this.ile.AutoSize = true;
            this.ile.Location = new System.Drawing.Point(6, 21);
            this.ile.Name = "ile";
            this.ile.Size = new System.Drawing.Size(65, 17);
            this.ile.TabIndex = 8;
            this.ile.Text = "Select File";
            this.ile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtOpenFile
            // 
            this.txtOpenFile.Location = new System.Drawing.Point(72, 16);
            this.txtOpenFile.Name = "txtOpenFile";
            this.txtOpenFile.Size = new System.Drawing.Size(458, 25);
            this.txtOpenFile.TabIndex = 3;
            // 
            // rbReadFromFile
            // 
            this.rbReadFromFile.AutoSize = true;
            this.rbReadFromFile.Location = new System.Drawing.Point(119, 25);
            this.rbReadFromFile.Name = "rbReadFromFile";
            this.rbReadFromFile.Size = new System.Drawing.Size(111, 21);
            this.rbReadFromFile.TabIndex = 2;
            this.rbReadFromFile.TabStop = true;
            this.rbReadFromFile.Text = "Read from File";
            this.rbReadFromFile.UseVisualStyleBackColor = true;
            this.rbReadFromFile.CheckedChanged += new System.EventHandler(this.rbReadFromFile_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRun);
            this.groupBox2.Controls.Add(this.cmbAlgorithm);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.rbTextualSim);
            this.groupBox2.Controls.Add(this.rbVisualSim);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtTextSimDisp);
            this.groupBox2.Location = new System.Drawing.Point(6, 195);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(648, 301);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Node IDs (In clockwise orientation):";
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(6, 243);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(636, 51);
            this.btnRun.TabIndex = 16;
            this.btnRun.Text = "RUN SIMULATION";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // cmbAlgorithm
            // 
            this.cmbAlgorithm.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAlgorithm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAlgorithm.FormattingEnabled = true;
            this.cmbAlgorithm.Location = new System.Drawing.Point(510, 204);
            this.cmbAlgorithm.Name = "cmbAlgorithm";
            this.cmbAlgorithm.Size = new System.Drawing.Size(132, 25);
            this.cmbAlgorithm.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(403, 205);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "Algorithm Type:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbTextualSim
            // 
            this.rbTextualSim.AutoSize = true;
            this.rbTextualSim.Location = new System.Drawing.Point(180, 205);
            this.rbTextualSim.Name = "rbTextualSim";
            this.rbTextualSim.Size = new System.Drawing.Size(66, 21);
            this.rbTextualSim.TabIndex = 13;
            this.rbTextualSim.TabStop = true;
            this.rbTextualSim.Text = "Textual";
            this.rbTextualSim.UseVisualStyleBackColor = true;
            // 
            // rbVisualSim
            // 
            this.rbVisualSim.AutoSize = true;
            this.rbVisualSim.Location = new System.Drawing.Point(114, 205);
            this.rbVisualSim.Name = "rbVisualSim";
            this.rbVisualSim.Size = new System.Drawing.Size(60, 21);
            this.rbVisualSim.TabIndex = 12;
            this.rbVisualSim.TabStop = true;
            this.rbVisualSim.Text = "Visual";
            this.rbVisualSim.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 205);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Simulation Type:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTextSimDisp
            // 
            this.txtTextSimDisp.Location = new System.Drawing.Point(6, 25);
            this.txtTextSimDisp.Multiline = true;
            this.txtTextSimDisp.Name = "txtTextSimDisp";
            this.txtTextSimDisp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTextSimDisp.Size = new System.Drawing.Size(636, 173);
            this.txtTextSimDisp.TabIndex = 7;
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(542, 163);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(106, 25);
            this.btnRead.TabIndex = 6;
            this.btnRead.Text = "Read";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // openFileNodeIds
            // 
            this.openFileNodeIds.FileName = "ring.csv";
            // 
            // DistributedAlgorithmSimulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 561);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "DistributedAlgorithmSimulator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Distributed Algorithm Simulator";
            this.Load += new System.EventHandler(this.SimulatorMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbInputIDs.ResumeLayout(false);
            this.gbInputIDs.PerformLayout();
            this.gbReadFromFile.ResumeLayout(false);
            this.gbReadFromFile.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.OpenFileDialog openFileNodeIds;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtTextSimDisp;
        private System.Windows.Forms.GroupBox gbInputIDs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInputIDs;
        private System.Windows.Forms.RadioButton rbInputIDs;
        private System.Windows.Forms.GroupBox gbReadFromFile;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Label ile;
        private System.Windows.Forms.TextBox txtOpenFile;
        private System.Windows.Forms.RadioButton rbReadFromFile;
        private System.Windows.Forms.ComboBox cmbAlgorithm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbTextualSim;
        private System.Windows.Forms.RadioButton rbVisualSim;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnExit;
    }
}

