﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/// <summary>
/// This namespace includes classes used for development of the application.
/// </summary>
namespace DistributedAlgorithmSimulator
{
    /// <summary>
    /// Extension methods used for various tasks.
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Makes a new copy of an object without keeping a reference.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="a">Object to copy</param>
        /// <returns>A new copy of an object of type T</returns>
        /// <remarks>The class of the object must be marked as [Serializable] to be able to DeepClone.</remarks>
        public static T DeepClone<T>(this T a)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, a);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Sets high quiality parameters to a Graphics object.
        /// </summary>
        /// <param name="g">Graphics object.</param>
        public static void SetHighQuality(this Graphics g)
        {
            g.CompositingMode = CompositingMode.SourceOver;
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
        }

        /// <summary>
        /// Draws a circle using given parameters on a Graphics object.
        /// </summary>
        /// <param name="g">Graphics object</param>
        /// <param name="pen">Pen to draw with</param>
        /// <param name="centerX">Circle center X-coordinate</param>
        /// <param name="centerY">Circle center Y-coordinate</param>
        /// <param name="radius">Circle radius</param>
        public static void DrawCircle(this Graphics g, Pen pen, float centerX, float centerY, float radius)
        {
            g.DrawEllipse(pen, centerX - radius, centerY - radius, radius + radius, radius + radius);
        }
    }
}
