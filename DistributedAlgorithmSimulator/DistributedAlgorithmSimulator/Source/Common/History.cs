﻿using System.Collections.Generic;
using WrapperClasses;

/// <summary>
/// This namespace includes classes used for development of the application.
/// </summary>
namespace DistributedAlgorithmSimulator
{
    /// <summary>
    /// Keeps a record of state of all the varialbes of the algorithm.
    /// One <see cref="History"/> object refers to one step in the execution of the algorithm.
    /// </summary>
    public class History
    {
        private int totalSteps;
        private int roundSteps;
        private int roundNum;

        private List<NodeState> stateList;

        /// <summary>
        /// Step number in the overall execution.
        /// </summary>
        public int TotalSteps
        {
            get { return totalSteps; }
            set { totalSteps = value; }
        }

        /// <summary>
        /// Step number in the current round of execution.
        /// </summary>
        public int RoundSteps
        {
            get { return roundSteps; }
            set { roundSteps = value; }
        }

        /// <summary>
        /// Round number in the execution.
        /// </summary>
        public int RoundNum
        {
            get { return roundNum; }
            set { roundNum = value; }
        }

        /// <summary>
        /// List of <see cref="NodeState"/> objects representing the current step.
        /// </summary>
        public List<NodeState> StateList
        {
            get { return stateList; }
            set { stateList = value; }
        }

        /// <summary>
        /// Initializes an instance of the <see cref="History"/> class.
        /// </summary>
        public History()
        {
            totalSteps = 0;
            roundSteps = 0;
            roundNum = 0;
            stateList = new List<NodeState>();
        }
    }
}
