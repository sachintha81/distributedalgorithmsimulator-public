﻿/// <summary>
/// This namespace includes classes used for development of the application.
/// </summary>
namespace DistributedAlgorithmSimulator
{
    /// <summary>
    /// Algorithm Type
    /// </summary>
    public enum Algorithm
    {
        /// <summary>LCR Algorithm</summary>
        LCR = 0,
        /// <summary>UniqueK Algorithm</summary>
        UniqueK = 1
    }

    /// <summary>
    /// Contains common fields.
    /// </summary>
    public static class Common
    {
        /// <summary>
        /// Sting: Algorithm
        /// </summary>
        public const string STR_ALG = "Algorithm";

        /// <summary>
        /// String: LCR
        /// </summary>
        public const string STR_ALG_LCR = "LCR";

        /// <summary>
        /// String: UniqueK
        /// </summary>
        public const string STR_ALG_UNIQUE_K = "UniqueK";

        /// <summary>
        /// String: Clockwise
        /// </summary>
        public const string STR_DIR_CW = "Clockwise";

        /// <summary>
        /// String: Counter Clockwise
        /// </summary>
        public const string STR_DIR_CCW = "Counter Clockwise";

        /// <summary>
        /// String: Finished!
        /// </summary>
        public const string STR_FINISHED = "FINISHED!";
    
        /// <summary>
        /// String: Textual Simulation
        /// </summary>
        public const string STR_TEXT_SIM_TITLE = "Textual Simulation : ";

        /// <summary>
        /// String: Visual Simulation
        /// </summary>
        public const string STR_VISUAL_SIM_TITLE = "Visual Simulation : ";

        /// <summary>
        /// String: SimulationResults (Default output file name without extension)
        /// </summary>
        public const string STR_DEFAULT_SAVE_FILE = "SimulationResults";

        /// <summary>
        /// Caption: ERROR!
        /// Used as general error message MessageBox caption.
        /// </summary>
        public const string CAPTION_ERROR = "ERROR!";

        /// <summary>
        /// Caption: FILE ERROR.
        /// Used as file error message MessabeBox caption.
        /// </summary>
        public const string CAPTION_FILE_ERROR = "FILE ERROR!";

        /// <summary>
        /// Caption: FILE WRITE.
        /// Used as file write error message MessageBox captioin.
        /// </summary>
        public const string CAPTION_FILE_WRITE = "FILE WRITE";

        /// <summary>
        /// Caption: TEXTUAL SIMULATION.
        /// Used as caption for prompt for textual simulation MessageBox.
        /// </summary>
        public const string CAPTION_TEXT_SIM = "TEXTUAL SIMULATION";

        /// <summary>
        /// Caption: INVALID ID ERROR.
        /// Used as caption for invalid ID error messages MessageBox.
        /// </summary>
        public const string CAPTION_INVALID_ID = "INVALID ID ERROR!";

        /// <summary>
        /// Success message.
        /// </summary>
        public const string ERR_MSG_SUCCESS = "SUCCESS";
    }
}