﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

/// <summary>
/// This namespace includes classes used for development of the application.
/// </summary>
namespace DistributedAlgorithmSimulator
{
    /// <summary>
    /// Static class offering file read/write operations tailored to Distributed Algorithm Simulator.
    /// </summary>
    public static class FileOperations
    {
        /// <summary>
        /// Reads a CSV file and store values in a list of integers.
        /// </summary>
        /// <returns><c>true</c>, if file was read successfully, <c>false</c> otherwise.</returns>
        /// <param name="fileName">File name.</param>
        /// <param name="idList">Node ID list.</param>
        /// <param name="errMsg">Error message. "SUCCESS" if no error.</param>
        public static bool ReadCSVFile(string fileName, ref List<int> idList, ref string errMsg)
        {
            try
            {
                FileStream fs = File.OpenRead(fileName);
                StreamReader sr = new StreamReader(fs);
                string line = sr.ReadLine();

                string[] sep = new string[1] { "," };
                string[] items = line.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < items.Length; i++)
                {
                    idList.Add(Convert.ToInt32(items[i]));
                }

                sr.Close();
                fs.Close();

                sr.Dispose();
                fs.Dispose();

                errMsg = Common.ERR_MSG_SUCCESS;
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Opens a SaveFileDialog to allow user to save the file.
        /// </summary>
        /// <returns><c>true</c>, if file save was successfully, <c>false</c> otherwise.</returns>
        /// <param name="defFileName">Default file name to save to.</param>
        /// <param name="content">Content to be saved.</param>
        /// <param name="errMsg">Error message. "SUCCESS" if no error.</param>
        /// <remarks>Valid file types: ".txt", ".log".</remarks>
        public static bool SaveToFile(string defFileName, string content, ref string errMsg)
        {
            FileStream fs = null;
            StreamWriter sw = null;

            try
            {
                // Displays a SaveFileDialog so the user can save the file
                SaveFileDialog saveFileTextSimDisp = new SaveFileDialog();
                saveFileTextSimDisp.Filter = "Text File|*.txt|Log File|*.log";
                saveFileTextSimDisp.DefaultExt = "Text File|*.txt";
                saveFileTextSimDisp.FileName = defFileName;
                saveFileTextSimDisp.Title = "Save to File";
                saveFileTextSimDisp.ShowDialog();
                
                // If the file name is not an empty string open it for saving.
                if (saveFileTextSimDisp.FileName != string.Empty)
                {
                    // Saves the content via a FileStream created by the OpenFile method.
                    fs = (FileStream)saveFileTextSimDisp.OpenFile();
                    sw = new StreamWriter(fs);

                    sw.Write(content);

                    sw.Close();
                    fs.Close();
                }

                sw.Dispose();
                fs.Dispose();

                errMsg = Common.ERR_MSG_SUCCESS;
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
            }
        }
    }
}
