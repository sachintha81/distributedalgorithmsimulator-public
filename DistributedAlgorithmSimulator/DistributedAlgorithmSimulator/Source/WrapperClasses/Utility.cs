﻿using System.Collections.Generic;

/// <summary>
/// Wrapper classes that fascilitate a template API for distributed classes in DistributedAlgorithms namespace.
/// </summary>
namespace WrapperClasses
{
    /// <summary>
    /// Common Utility class.
    /// Classes of different algorithms must override these and perform appropriate changes.
    /// </summary>
    abstract public class Utility
    {
        /// <summary>
        /// Successful operation.
        /// </summary>
        public const string ERR_MSG_SUCCESS = "SUCCESS";

        /// <summary>
        /// Checks for errors.
        /// </summary>
        /// <returns><c>true</c>, if no errors, <c>false</c> otherwise.</returns>
        /// <param name="n">Number of nodes in the ring.</param>
        /// <param name="uidList">List of ID representing the nodes in the ring.</param>
        /// <param name="errMsg">Error message, if any. "SUCCESS" if successful.</param>
        abstract public bool ErrorChecks(int n, List<int> uidList, ref string errMsg);

        /// <summary>
        /// Performs required initializations
        /// </summary>
        /// <param name="n">Size of the ring</param>
        /// <param name="nodeList">Node list</param>
        /// <param name="msgList">Message Buffer</param>
        /// <param name="errMsg">Error message, if any. "SUCCESS" if successful.</param>
        /// <returns><see langword="true"/>: Initialization successfu. <see langword="false"/>: otherwise.</returns>
        abstract public bool Initialize(int n, ref List<Node> nodeList, ref List<Message> msgList, ref string errMsg);

        /// <summary>
        /// Checks whether the algorithm execution is finished.
        /// </summary>
        /// <returns><c>true</c> if executioin is finished; otherwise, <c>false</c>.</returns>
        /// <param name="n">Ring size</param>
        /// <param name="nodeList">Node list</param>
        abstract public bool IsFinished(int n, List<Node> nodeList);

        /// <summary>
        /// Prints the ring orientation in clockwise order.
        /// </summary>
        /// <returns><c>true</c>, if list was printed, <c>false</c> otherwise.</returns>
        /// <param name="n">Ring size</param>
        /// <param name="nodeList">Node list.</param>
        /// <param name="outputString">Ring orientation in string format.</param>
        /// <param name="errMsg">Error message, if any. "SUCCESS" if successful.</param>
        abstract public bool PrintList(int n, List<Node> nodeList, ref string outputString, ref string errMsg);

        /// <summary>
        /// Returns (as a reference) the state of all the nodes as a single string.
        /// </summary>
        /// <param name="n">Ring size</param>
        /// <param name="step">The step number to be printed.</param>
        /// <param name="nodeList">Node list.</param>
        /// <param name="outputString">The state of all the nodes as a single string.</param>
        /// <param name="errMsg">Error message, if any. "SUCCESS" if successful.</param>
        abstract public void PrintStep(int n, int step, List<Node> nodeList, ref string outputString, ref string errMsg);

        /// <summary>
        /// Returns (as a reference) the state of all the nodes as a <see cref="NodeState"/> list.
        /// </summary>
        /// <param name="n">Ring size</param>
        /// <param name="nodeList">Node list</param>
        /// <param name="stateList">List of node states</param>
        abstract public void GetStatus(int n, List<Node> nodeList, ref List<NodeState> stateList);

        /// <summary>
        /// Gets a list of variables that the algorithm uses.
        /// </summary>
        /// <returns>A lit of variables used in the algorithm.</returns>
        abstract public List<NodeState.Item> GetVariables();
    }
}
