﻿using System;

/// <summary>
/// Wrapper classes that fascilitate a template API for distributed classes in DistributedAlgorithms namespace.
/// </summary>
namespace WrapperClasses
{
    /// <summary>
    /// MESSAGE super class. Used in the SimulatorMain.
    /// Each algorithm will derive from this to define its own message.
    /// </summary>
    [Serializable] abstract public class Message
    {
    }
}
