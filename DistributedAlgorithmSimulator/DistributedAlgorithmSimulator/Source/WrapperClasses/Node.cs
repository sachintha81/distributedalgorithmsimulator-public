﻿using System;
using System.Collections.Generic;

/// <summary>
/// Wrapper classes that fascilitate a template API for distributed classes in DistributedAlgorithms namespace.
/// </summary>
namespace WrapperClasses
{
    /// <summary>
    /// Node super class. Used in the SimulatorMain.
    /// Each algorithm's libraries will derive from this to implement the node.
    /// </summary>
    [Serializable] abstract public class Node
    {
        private int linkId;

        /// <summary>
        /// The ID used to create the network. Different from node IDs.
        /// </summary>
        public int LinkId
        {
            get { return linkId; }
            set { linkId = value; }
        }

        private int cwNeighbor;

        /// <summary>
        /// LinkID of the clockwise neighbor.
        /// </summary>
        public int CwNeighbor
        {
            get { return cwNeighbor; }
            set { cwNeighbor = value; }
        }

        private int ccwNeighbor;

        /// <summary>
        /// LinkID of the counter-clockwise neighbor.
        /// </summary>
        public int CcwNeighbor
        {
            get { return ccwNeighbor; }
            set { ccwNeighbor = value; }
        }

        private Message rcvdMsg = null;

        /// <summary>
        /// Received message at each step.
        /// </summary>
        public Message RcvdMsg
        {
            get { return rcvdMsg; }
            set { rcvdMsg = value; }
        }

        private Message sentMsg = null;

        /// <summary>
        /// Sent message at the end of each step.
        /// </summary>
        public Message SentMsg
        {
            get { return sentMsg; }
            set { sentMsg = value; }
        }

        /// <summary>
        /// Indicates whether the algorithm execution has started or not
        /// </summary>
        protected bool hasStarted = false;

        /// <summary>
        /// Any particular node's relavant index in the message queue
        /// </summary>
        protected int selfIndex = -1;

        /// <summary>
        /// A nodes CW neighbor in the message queue
        /// </summary>
        protected int cwIndex = -1;

        /// <summary>
        /// A nodes CCW neighbor in the message queue
        /// </summary>
        protected int ccwIndex = -1;

        /// <summary>
        /// Node ID. May or may not be unique.
        /// </summary>
        protected int uid = -1;

        /// <summary>
        /// Node IDs of nodes.
        /// </summary>
        public int UID
        {
            get { return uid; }
            set { uid = value; }
        }

        /// <summary>
        /// Actions performed at each time step.
        /// This is the coded Actions Table.
        /// </summary>
        /// <param name="originalList">List of messages received by all nodes in the current step.</param>
        /// <param name="msgLst">List of messages that whill be sent by all nodes at the end of the current step.</param>
        abstract public void TimeTick(List<Message> originalList, ref List<Message> msgLst);
    }
}
