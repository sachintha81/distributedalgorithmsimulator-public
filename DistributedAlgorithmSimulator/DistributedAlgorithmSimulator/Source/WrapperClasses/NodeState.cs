﻿using System;
using System.Collections.Generic;

/// <summary>
/// Wrapper classes that fascilitate a template API for distributed classes in DistributedAlgorithms namespace.
/// </summary>
namespace WrapperClasses
{
    /// <summary>
    /// Represents a state of variables of a node during the execution of the algorithm.
    /// </summary>
    [Serializable] public class NodeState
    {
        /// <summary>
        /// Represents a variable of a node.
        /// </summary>
        public struct Item
        {
            private string itemName;

            /// <summary>
            /// Variable name.
            /// </summary>
            public string ItemName
            {
                get { return itemName; }
                set { itemName = value; }
            }

            private object itemValue;

            /// <summary>
            /// Variable value. Converted to object type.
            /// </summary>
            public object ItemValue
            {
                get { return itemValue; }
                set { itemValue = value; }
            }
        }

        private int linkId;

        /// <summary>
        /// The ID used to create the network. Different from node IDs.
        /// </summary>
        public int LinkId
        {
            get { return linkId; }
            set { linkId = value; }
        }

        private int cwId;

        /// <summary>
        /// LinkID of the clockwise neighbor.
        /// </summary>
        public int CwId
        {
            get { return cwId; }
            set { cwId = value; }
        }

        private int ccwId;

        /// <summary>
        /// LinkID of the counter-clockwise neighbor.
        /// </summary>
        public int CcwId
        {
            get { return ccwId; }
            set { ccwId = value; }
        }

        List<Item> itemList = null;

        /// <summary>
        /// List of variables.
        /// </summary>
        public List<Item> ItemList
        {
            get { return itemList; }
            set { itemList = value; }
        }

        /// <summary>
        /// Initializes a new instance of <see cref="NodeState"/> class.
        /// </summary>
        /// <param name="lid">LinkID.</param>
        /// <param name="cw">LinkID of clockwise neighbor.</param>
        /// <param name="ccw">LinkID of counter-clockwise neighbor.</param>
        public NodeState(int lid, int cw, int ccw)
        {
            linkId = lid;
            cwId = cw;
            ccwId = ccw;
            itemList = new List<Item>();
        }

        /// <summary>
        /// Adds an item to the Item List.
        /// </summary>
        /// <param name="it">Item to add.</param>
        public void AddItem(Item it)
        {
            itemList.Add(it);
        }
    }
}
