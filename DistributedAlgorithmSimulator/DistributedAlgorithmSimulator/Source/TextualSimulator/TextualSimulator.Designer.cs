﻿namespace DistributedAlgorithmSimulator
{
    partial class TextualSimulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextualSimulator));
            this.gbTextSim = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numUpDownSpeed = new System.Windows.Forms.NumericUpDown();
            this.txtRing = new System.Windows.Forms.TextBox();
            this.btnRunTextSim = new System.Windows.Forms.Button();
            this.txtTextSim = new System.Windows.Forms.TextBox();
            this.btnSaveTextSim = new System.Windows.Forms.Button();
            this.btnResetTextSim = new System.Windows.Forms.Button();
            this.btnExitTextSim = new System.Windows.Forms.Button();
            this.gbTextSim.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // gbTextSim
            // 
            this.gbTextSim.Controls.Add(this.label1);
            this.gbTextSim.Controls.Add(this.numUpDownSpeed);
            this.gbTextSim.Controls.Add(this.txtRing);
            this.gbTextSim.Controls.Add(this.btnRunTextSim);
            this.gbTextSim.Controls.Add(this.txtTextSim);
            this.gbTextSim.Controls.Add(this.btnSaveTextSim);
            this.gbTextSim.Controls.Add(this.btnResetTextSim);
            this.gbTextSim.Location = new System.Drawing.Point(12, 13);
            this.gbTextSim.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbTextSim.Name = "gbTextSim";
            this.gbTextSim.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbTextSim.Size = new System.Drawing.Size(760, 504);
            this.gbTextSim.TabIndex = 0;
            this.gbTextSim.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(262, 476);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 20;
            this.label1.Text = "Speed (ms):";
            // 
            // numUpDownSpeed
            // 
            this.numUpDownSpeed.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numUpDownSpeed.Location = new System.Drawing.Point(342, 472);
            this.numUpDownSpeed.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numUpDownSpeed.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numUpDownSpeed.Name = "numUpDownSpeed";
            this.numUpDownSpeed.Size = new System.Drawing.Size(104, 25);
            this.numUpDownSpeed.TabIndex = 3;
            this.numUpDownSpeed.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUpDownSpeed.ValueChanged += new System.EventHandler(this.numUpDownSpeed_ValueChanged);
            // 
            // txtRing
            // 
            this.txtRing.AcceptsTab = true;
            this.txtRing.Location = new System.Drawing.Point(6, 25);
            this.txtRing.Multiline = true;
            this.txtRing.Name = "txtRing";
            this.txtRing.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRing.Size = new System.Drawing.Size(748, 78);
            this.txtRing.TabIndex = 18;
            this.txtRing.TabStop = false;
            // 
            // btnRunTextSim
            // 
            this.btnRunTextSim.Location = new System.Drawing.Point(6, 472);
            this.btnRunTextSim.Name = "btnRunTextSim";
            this.btnRunTextSim.Size = new System.Drawing.Size(104, 25);
            this.btnRunTextSim.TabIndex = 1;
            this.btnRunTextSim.Text = "Run";
            this.btnRunTextSim.UseVisualStyleBackColor = true;
            this.btnRunTextSim.Click += new System.EventHandler(this.btnRunTextSim_Click);
            // 
            // txtTextSim
            // 
            this.txtTextSim.AcceptsTab = true;
            this.txtTextSim.Location = new System.Drawing.Point(6, 109);
            this.txtTextSim.Multiline = true;
            this.txtTextSim.Name = "txtTextSim";
            this.txtTextSim.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTextSim.Size = new System.Drawing.Size(748, 357);
            this.txtTextSim.TabIndex = 16;
            this.txtTextSim.TabStop = false;
            // 
            // btnSaveTextSim
            // 
            this.btnSaveTextSim.Location = new System.Drawing.Point(650, 472);
            this.btnSaveTextSim.Name = "btnSaveTextSim";
            this.btnSaveTextSim.Size = new System.Drawing.Size(104, 25);
            this.btnSaveTextSim.TabIndex = 4;
            this.btnSaveTextSim.Text = "Save to File";
            this.btnSaveTextSim.UseVisualStyleBackColor = true;
            this.btnSaveTextSim.Click += new System.EventHandler(this.btnSaveTextSim_Click);
            // 
            // btnResetTextSim
            // 
            this.btnResetTextSim.Location = new System.Drawing.Point(116, 472);
            this.btnResetTextSim.Name = "btnResetTextSim";
            this.btnResetTextSim.Size = new System.Drawing.Size(104, 25);
            this.btnResetTextSim.TabIndex = 2;
            this.btnResetTextSim.Text = "Reset";
            this.btnResetTextSim.UseVisualStyleBackColor = true;
            this.btnResetTextSim.Click += new System.EventHandler(this.btnResetTextSim_Click);
            // 
            // btnExitTextSim
            // 
            this.btnExitTextSim.Location = new System.Drawing.Point(662, 524);
            this.btnExitTextSim.Name = "btnExitTextSim";
            this.btnExitTextSim.Size = new System.Drawing.Size(104, 25);
            this.btnExitTextSim.TabIndex = 5;
            this.btnExitTextSim.Text = "EXIT";
            this.btnExitTextSim.UseVisualStyleBackColor = true;
            this.btnExitTextSim.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // TextualSimulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btnExitTextSim);
            this.Controls.Add(this.gbTextSim);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "TextualSimulator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Textual Simulator";
            this.gbTextSim.ResumeLayout(false);
            this.gbTextSim.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownSpeed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbTextSim;
        private System.Windows.Forms.Button btnExitTextSim;
        private System.Windows.Forms.Button btnSaveTextSim;
        private System.Windows.Forms.Button btnResetTextSim;
        private System.Windows.Forms.Button btnRunTextSim;
        private System.Windows.Forms.TextBox txtTextSim;
        private System.Windows.Forms.TextBox txtRing;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numUpDownSpeed;
    }
}