﻿using DistributedAlgorithms;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

using WrapperClasses;

/// <summary>
/// This namespace includes classes used for development of the application.
/// </summary>
namespace DistributedAlgorithmSimulator
{
    /// <summary>
    /// Textual Simulator window.
    /// </summary>
    public partial class TextualSimulator : Form
    {
        private const string LABEL_RUN = "Run";
        private const string LABEL_PAUSE = "Pause";

        /// <summary>
        /// Lists to keep original data passed to the class
        /// </summary>
        private List<int> oIdList = null;
        private List<Node> oNodeList = null;
        private List<WrapperClasses.Message> oMsgBuff = null;

        /// <summary>
        /// Working copies of data
        /// </summary>
        private List<int> IdList = null;
        private List<Node> NodeList = null;
        private List<WrapperClasses.Message> MsgBuff = null;
        private int N;

        // Name of the algorithm
        private string oAlgorithm = String.Empty;

        // Utility class
        private Utility util = null;

        // Timer object
        private System.Threading.Timer t = null;

        // Time Interval of the timer
        private int timeInterval = 1000;

        // Lock object
        private static Object printLock = new Object();
        
        // Number of time-steps the program is run
        private static int timeStep = 0;

        // Current status: Execution Finished or Not
        private bool isFinished = false;

        /// <summary>
        /// Initializes an instance of the <see cref="TextualSimulator"/> class.
        /// </summary>
        /// <param name="idList">Node ID list.</param>
        /// <param name="nodeList">Node list.</param>
        /// <param name="msgList">Message buffer.</param>
        /// <param name="n">Size of the ring in terms of nodes.</param>
        /// <param name="alg">Algorithm type.</param>
        public TextualSimulator(List<int> idList, List<WrapperClasses.Node> nodeList, List<WrapperClasses.Message> msgList, int n, string alg)
        {
            InitializeComponent();

            // Make new copies (with no references) of lists to be used after a reset.
            oIdList = idList.DeepClone();
            oNodeList = nodeList.DeepClone();
            oMsgBuff = msgList.DeepClone();

            IdList = oIdList.DeepClone();
            NodeList = oNodeList.DeepClone();
            MsgBuff = oMsgBuff.DeepClone();

            N = n;

            oAlgorithm = String.Copy(alg);

            string DISP_MSG = string.Empty;
            string ERR_MSG = string.Empty;

            // Creates an instance of the Utility class depending on the algorithm type.
            switch (oAlgorithm)
            {
                case Common.STR_ALG_LCR:
                    util = new LCRUtility();
                    break;
                case Common.STR_ALG_UNIQUE_K:
                    util = new UniqueKUtility();
                    break;
            }

            //	Performs necessary initializations
            if (!util.Initialize(N, ref NodeList, ref MsgBuff, ref ERR_MSG))
            {
                MessageBox.Show(ERR_MSG, Common.CAPTION_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            //	Prints the initial list configuration
            if (!util.PrintList(N, NodeList, ref DISP_MSG, ref ERR_MSG))
            {
                MessageBox.Show(ERR_MSG, Common.CAPTION_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                txtRing.Text = DISP_MSG;
            }

            gbTextSim.Text = Common.STR_TEXT_SIM_TITLE + alg;
            btnRunTextSim.Text = LABEL_RUN;
            isFinished = false;
        }

        /// <summary>
        /// Performs one step of the execution of the algorith.
        /// Called from the <see cref="System.Threading.Timer"/>, at each time tick.
        /// </summary>
        /// <param name="o">Parameters. None here.</param>
        private void TimerCallback(Object o)
        {
            string DISP_MSG = string.Empty;
            string ERR_MSG = string.Empty;

            // Original represents the messages received at the current step.
            List<WrapperClasses.Message> Original = new List<WrapperClasses.Message>(N);
            for (int i = 0; i < N; i++)
            {
                Original.Add(MsgBuff[i]);
            }

            // This part has to be done in a lock. Otherwise print statement can get mixed up.
            lock (printLock)
            {
                // Calls the algorithm's each step execution method.
                // Original contains received messages, and the algorithm updates MsgBuff with the messages to be sent.
                for (int i = 0; i < N; i++)
                {
                    NodeList[i].TimeTick(Original, ref MsgBuff);
                }

                // Gets the status of the variables after above step and displays on a textbox.
                util.PrintStep(N, timeStep, NodeList, ref DISP_MSG, ref ERR_MSG);
                AppendText(txtTextSim, DISP_MSG);

                timeStep = timeStep + 1;
            }

            // Check whether the execution is finished.
            // If true, dispose the timer and do the necessary.
            if (util.IsFinished(N, NodeList))
            {
                isFinished = true;
                AppendText(txtTextSim, System.Environment.NewLine);
                AppendText(txtTextSim, Common.STR_FINISHED);
                SetText(btnRunTextSim, LABEL_RUN);
                t.Dispose();
            }
        }

        /// <summary>
        /// Resets all varibles to initial state.
        /// </summary>
        private void Reset()
        {
            isFinished = false;
            if (t != null)
            {
                t.Dispose();
                t = null;
            }

            string DISP_MSG = string.Empty;
            string ERR_MSG = string.Empty;

            IdList = oIdList.DeepClone();
            NodeList = oNodeList.DeepClone();
            MsgBuff = oMsgBuff.DeepClone();

            //	Performs necessary initializations
            if (!util.Initialize(N, ref NodeList, ref MsgBuff, ref ERR_MSG))
            {
                MessageBox.Show(ERR_MSG, Common.CAPTION_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            btnRunTextSim.Text = LABEL_RUN;
            txtTextSim.Text = string.Empty;
            timeStep = 0;
            timeInterval = 1000;
            numUpDownSpeed.Value = timeInterval;
        }

        /// <summary>
        /// Toggles Run/Pause button.
        /// </summary>
        private void ToggleRunPause()
        {
            if (btnRunTextSim.Text == LABEL_RUN)
            {
                btnRunTextSim.Text = LABEL_PAUSE;
                t = new System.Threading.Timer(TimerCallback, null, 0, timeInterval);
            }
            else
            {
                btnRunTextSim.Text = LABEL_RUN;
                t.Dispose();
                t = null;
            }
        }

        // Following are delegates to change controls from the TimerCallback thread.
        #region

        delegate void SetTextCallback(Control ctrl, string text);
        private void SetText(Control ctrl, string text)
        {
            if (ctrl.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { ctrl, text });
            }
            else
            {
                ctrl.Text = text;
            }
        }

        delegate void AppendTextCallback(TextBox tb, string text);
        private void AppendText(TextBox tb, string text)
        {
            if (tb.InvokeRequired)
            {
                AppendTextCallback d = new AppendTextCallback(AppendText);
                this.Invoke(d, new object[] { tb, text });
            }
            else
            {
                tb.AppendText(text);
            }
        }

        #endregion

        /// <summary>
        /// Resets the execution.
        /// </summary>
        /// <param name="sender">[Reset] button.</param>
        /// <param name="e">Event data.</param>
        private void btnResetTextSim_Click(object sender, EventArgs e)
        {
            Reset();
        }

        /// <summary>
        /// Saves execution results to a file.
        /// </summary>
        /// <param name="sender">[Save to File] button.</param>
        /// <param name="e">Event data.</param>
        private void btnSaveTextSim_Click(object sender, EventArgs e)
        {
            string ERR_MSG = string.Empty;
            if (!FileOperations.SaveToFile(Common.STR_DEFAULT_SAVE_FILE, txtTextSim.Text, ref ERR_MSG))
            {
                MessageBox.Show(ERR_MSG, Common.CAPTION_FILE_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Run the execution.
        /// </summary>
        /// <param name="sender">[Run/Pause] button.</param>
        /// <param name="e">Event data.</param>
        private void btnRunTextSim_Click(object sender, EventArgs e)
        {
            if (isFinished)
            {
                isFinished = false;
                Reset();
            }
            ToggleRunPause();
        }

        /// <summary>
        /// Change the execution speed based on NumericUpDown control value.
        /// </summary>
        /// <param name="sender">[Speed] numeric up down control.</param>
        /// <param name="e">Event data.</param>
        private void numUpDownSpeed_ValueChanged(object sender, EventArgs e)
        {
            int oldInterval = timeInterval;
            timeInterval = Convert.ToInt32(numUpDownSpeed.Value);
            if (t != null)
            {
                t.Change(oldInterval, timeInterval);
            }
        }

        /// <summary>
        /// Exits the Textual Simulation window.
        /// </summary>
        /// <param name="sender">[Exit] button.</param>
        /// <param name="e">Event data.</param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (t != null)
            {
                t.Dispose();
                t = null;
            }
            GC.Collect();
            this.Close();
        }
    }
}
