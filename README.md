# README #

### What is this repository for? ###

* This is a simulator application for distributed leader election algorithms.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

# Distributed Algorithm Simulator

This is a simulator application for distributed leader election algorithms. Developed on the Microsoft .NET 4.5 framework using Visual C#. To work, it needs .NET 4.5 framework and has been tested on Windows 8 and above.

This application simulates the execution of distributed leader election algorithms in ring networks. We have chosen the following two algorithms to implement.

* LCR Algorithm
* UNIQUE_k Algorithm

Both those algorithms are algorithms for unidirectional algorithms, but this application can simulate bi-directional algorithms as well.


#Overview of Contents

##DistributedAlgorithmSimulator
* Contains the entire project source code.
* Includes the Setup project.
* Contains complete documentation in HTML and CHM formats.

##DistributedAlgorithmSimulator-Documentation
* Contains complete documentation in HTML, CHM, and Word formats.

##DistributedAlgorithmSimulator-Setup
* Contains Setup binaries.

##DistributedAlgorithmSimulator-UserGuide
* A complete user guide from installation to running the application.

### Who do I talk to? ###

* Sachintha Gurudeniya 
* EMail: [sachintha81@gmail.com](mailto:sachintha81@gmail.com)
* www.sach.site